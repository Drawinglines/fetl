from setuptools import setup

setup(name='fetl',
      version='2.0.2',
      description='terra.ai forecasting ETL module',
      url='https://bitbucket.org/Drawinglines/fetl/src',
      author='rob',
      author_email='rob@terra.ai',
      license='terra.ai',
      packages=['batch','cluster','combinatorics', 'conns', 'extract','helpers','load', 'tag', 'transform'],
	  include_package_data=True,
      zip_safe=False)