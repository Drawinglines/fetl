# -*- coding: utf-8 -*-
"""
Created on Fri May 06 13:41:37 2016
Pre-processing package

@author: Aamir
"""

import time
# from collections import defaultdict
from multiprocessing import Pool

from extract.pp_helpers import get_oilprices, get_well_ids, describe_table
from extract.pp_helpers import get_well_data, get_static_data, ld_to_dl
from extract.pp_helpers import process_data, establish_db_conn, get_well_data_ts

#from bprofile import BProfile

__version__ = 1.52

#Global parameters
PROD_SCHEMA = "public_data"
DB_PRODTABLE_NAME = "bigprod_ts" #"big_prod_sum11"

STATIC_SCHEMA = "public_data"
DB_MASTERTABLE_NAME = "big_master_detail"

_CONN, _CURSOR = establish_db_conn()

#Check if Timeseries table
_TIMESERIES_FLAG = False
if "_ts" in DB_PRODTABLE_NAME:
    _TIMESERIES_FLAG = True


def __preprocessor(inp):

    well, time_trsh, prod_trsh, diff_days, column_names, structure,\
    static_col_names, static_structure,\
    oil_prices_full, flag_dict = inp[0], inp[1], inp[2], inp[3], inp[4],\
    inp[5], inp[6], inp[7], inp[8], inp[9]
    #print "Reading well id -", well

    if _TIMESERIES_FLAG:
        data = get_well_data_ts(_CURSOR, structure, column_names, well,
                                PROD_SCHEMA, DB_PRODTABLE_NAME)
    else:
        data = get_well_data(_CURSOR, structure, column_names, well,
                             PROD_SCHEMA, DB_PRODTABLE_NAME)

    prod_data = process_data(_CURSOR, data, time_trsh, prod_trsh, diff_days, oil_prices_full, flag_dict)

    if prod_data["accpt_flag"] > 2:
        prod_data = {"API_prod":[well], "accpt_flag":prod_data["accpt_flag"], "reason":prod_data["reason"]}

    if static_col_names is not None and prod_data != 0:
        static_data = get_static_data(_CURSOR, static_structure, static_col_names,
                                      well, STATIC_SCHEMA,
                                      DB_MASTERTABLE_NAME)
    else:
        static_data = []

    return prod_data, static_data


def start_preprocess(state_api=33, return_format="L_of_D", mp_flag=True,
                     verbosity=True, static_attr=True, time_trsh={"oil":4, "gas":4},
                     prod_trsh={"oil":3, "gas":3}, diff_days=2, well_ids=[]):
    """
    Kicks off preprocessing of wells and returns the data,alongwith list of
    well apis that are worthy of analysis and not worthy of analysis.

    Args:
        state_api : Integer object. State whose wells should be preprocessed

        return_format : String object. Format to return data, list of dictionary
                        OR dictionary of lists. Values can be - 'L_of_D' or 'D_of_L'.
        mp : Boolean object. If set to True multiprocessing will be used to
             speed up preprocessing

        verbosity : Boolean object. If true func will print summary of func
                    execution to console.

        static_attr : Boolean object. If true, then a static data dictionary
                      will be returned as well.

        time_trsh : dictionary. Nos of month the well should
                    have oil production to be used for analysis

        diff_days : Integer object. Threshold on number of down-times,
                    to be considered as an actual failure point.

        well_ids : List of Integer objects. Provide specific wells ids to be processed,
                   instead of whole state. In this case state_api value will be disregarded.

    Returns:
        prod_final : Contains the preprocessed well production data. Format depends
                on parameter passed for return_format. If empty then no wells
                worth analysing. May include all or some of the following keys-

                'API_prod', 'state_API' : Well API, State API
                'oil_mo', 'wtr_mo', 'gas_mo' : Monthly production values
                'oil_pd', 'wtr_pd' : Daily production values
                'prod_days' : No of days produced each month
                'oilprice' : Price of oil for well's report dates
                'report_date_mo' : Production dates
                'MonthIndx' : Index numbers for report date

        static_final : It will return static attributes about a well. If static_attr parameter is false it will be empty.
                       It relates to prod_final by index.
                       Check "Static attributes dictionary" confluence page
                       for a description of the attributes.

        api_status : It identifies which wells have been preprocessed and returned in the prod_final object,
                    and the wells that are not good for analysis along with the reason. 
                    You need to write this to the database using the write_DB() while pushing results.
    """
    flag_dict = {0: 'Fully producing well', 1: 'No oil but significant gas',
                 2: 'Low production well', 3: 'No significant oil or gas',
                 4: 'Injector well', 5: 'Inconsistent well with the logic',
                 6: 'No data available', 7:'Produced less than time threshold',
                 8: 'Well abandoned'}
                 
    _CONN, _CURSOR = establish_db_conn()

    start_time = time.time()

    column_names, structure = describe_table(_CURSOR, PROD_SCHEMA, DB_PRODTABLE_NAME)

    if static_attr:
        static_col_names, static_structure = describe_table(_CURSOR, STATIC_SCHEMA, DB_MASTERTABLE_NAME)
    else:
        static_col_names, static_structure = None, None


    if len(well_ids) == 0:
        well_ids = get_well_ids(_CURSOR, column_names, state_api, PROD_SCHEMA, DB_PRODTABLE_NAME)

    oil_prices_full = get_oilprices(_CURSOR, schema="public_data", tablename="oil_price_monthly")

    not_worthy_api, worthy_api, results, prod_final, static_final = [], [], [], [], []

    input_tuple = tuple((API, time_trsh, prod_trsh, diff_days, column_names, structure,
                         static_col_names, static_structure,
                         oil_prices_full, flag_dict) for API in well_ids)

    if verbosity:
        print "Pre-processing has begun -"

    if mp_flag:
        #Multiprocessing
        pool = Pool()
        tasks = pool.map_async(__preprocessor, input_tuple)

        tasks.wait()
        results = tasks.get()

        pool.close()
        pool.join()

    else:
        #Sequential
        for inp in input_tuple:
            data = __preprocessor(inp)
            results.append(data)

    api_status = {"API_prod":[], "state_api":[], "well_fore_status":[], "flag":[], "reason":[]}

    for data in results:
        #if len(data[0]) == 1 or type(data[0]) == dict:
        api_status["API_prod"].append([data[0]["API_prod"][0]])
        api_status["state_API"].append([state_api])

        if data[0]["accpt_flag"] > 1:
            #print "Well not worthy of analysis -",data["API_prod"]
            not_worthy_api.append(data[0]["API_prod"][0])
            api_status["well_fore_status"].append([0])
            api_status["flag"].append([data[0]["accpt_flag"]])
            api_status["reason"].append([data[0]["reason"]])
        else:
            worthy_api.append(data[0]["API_prod"][0])
            api_status["well_fore_status"].append([1])
            api_status["flag"].append([data[0]["accpt_flag"]])
            api_status["reason"].append([data[0]["reason"]])
            data[0].pop("accpt_flag")
            data[0].pop("reason")
            prod_final.append(data[0])
            static_final.append(data[1])

    if return_format == "D_of_L":
        prod_final = ld_to_dl(prod_final)
        if static_attr:
            static_final = ld_to_dl(static_final)

    if verbosity:
        print "-"*59
        print "Total number of wells -", len(well_ids)
        print "Not worthy of analysis -", len(not_worthy_api)
        print "Total Time taken- "+ str(round((time.time()-start_time), 2))+" secs"

#    if bool(prod_final):
#        write_DB(_CONN, _CURSOR, prod_final, verbosity, schema="aamir",
#             table_name="9_14_Test_preprocess", drop_table=True)
#
#        create_index(_CONN, _CURSOR, True, "aamir", "9_14_Test_preprocess", "API_prod", "test_index_9_14_apiprod")
#
#    if static_attr & bool(static_final):
#        write_DB(_CONN, _CURSOR, static_final, verbosity, schema="aamir",
#                 table_name="9_14_Test_preprocess_static", drop_table=True)
#
    #Write apis to DB
#    write_DB(_CONN, _CURSOR, api_status, verbosity, schema="aamir",
#              table_name="9_16_Well_status", drop_table=False)
##
##    write_metadata(_CONN, _CURSOR, "9_13_test", "FFR", "1.0", "2009-08-11", "Test table", "Forecast result")
#
#
#    _CURSOR.close()
#    _CONN.close()
#
#
#    return prod_final, static_final, api_status #worthy_api, not_worthy_api,
#
#
#if __name__ == '__main__':
#
#    for i in [50, 16, 43, 3, 4, 5, 49, 33, 25,30, 40]: #33, 40, 25,30, 50, 16, 43, 3, 4, 5, 49
#        print i
#        start_preprocess(state_api=i, return_format="L_of_D", mp_flag=True,
#                         verbosity=True, static_attr=True, well_ids=[], time_trsh={"oil":36, "gas":36},
#                          prod_trsh={"oil":3, "gas":3})
#    #print "-"*59
#    print
