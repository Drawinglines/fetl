import numpy as np


def preprocess(data, FEATURES, target, log):
    """ This function implements the most basic preprocessing for the data.
    It does the followings:
    # Makes sure that all of the values are finite.
    # Converts monthly oil and water to daily via dividing them by `proddays`.
    # Takes the log of oil and water.

    NOTE: This function is the only function that changes the global variable `data`
    after 'load_data()'.
    """
    if target not in FEATURES.keys():
        FEATURES[target] = 'ts'
    for i in range(len(data['oil'])):
        # For multi-dimensional forecasts
        if target is 'wor':
            data['wor'].append(data['water'][i]/data['oil'][i])
        elif target is 'cumoil':
            data['cumoil'][i].append(np.cumsum(data['oil'][i]))
        elif target is 'cumwater':
            data['cumwater'][i].append(np.cumsum(data['cumwater'][i]))
        # Main variables
        data['proddays'][i][np.isnan(data['proddays'][i])] = 0
        # data['oil'][i] /= data['proddays'][i]        # This makes oil per day (oil_pd)
        # data['water'][i] /= data['proddays'][i]      # This makes water per day (water_pd)
        # Take log of oil
        data['oil'][i] = np.log10(1+data['oil'][i])  # log(1+x) to make sure that it is invertable
        data['oil'][i][~np.isfinite(data['oil'][i])] = 0
        data['water'][i] = np.log10(1+data['water'][i]) # log(1+x) to make sure invertablity
        data['water'][i][~np.isfinite(data['water'][i])] = 0
        data['gas'][i] = np.log10(1+data['gas'][i]) # log(1+x) to make sure invertablity
        data['gas'][i][~np.isfinite(data['gas'][i])] = 0
        data[target][i][~np.isfinite(data[target][i])] = 0