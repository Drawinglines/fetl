



def mynum(func, a, replacement=0):
    ''' This function is only a way to get around the unexpected data while converting
    it to type specified by 'func' argument.
    You may want to write a more sophisticated function for treatment of bad entries.'''
    try:
        return func(a)
    except ValueError:
        # print('Expected float, got {} instead'.format(type(a)))
        return replacement