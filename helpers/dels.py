
from __future__ import division
from datetime import datetime, date
from calendar import monthrange
import pdb


# Need to understand how this is used....
initDate = datetime.strptime('1900-01-01' , '%Y-%m-%d')
indexDate = datetime.strptime('2008-01-01' , '%Y-%m-%d')

## helpers_pg - in use

def diff_month(date_in):
    """
    Calculates the difference (date_in-initDate) in terms of number of months in between them.
    """
    try:
        if not isinstance(date_in, datetime): # Parameter overloading
            date_in = datetime.strptime(date_in, '%Y-%m-%d')
        return (date_in.year - initDate.year)*12 + date_in.month - initDate.month
    except ValueError:
        pdb.set_trace()
        print "ValueError: The aregument was {}.".format(date_in)
        return 0

##  pp_helpers - Not used
def diff_month(date1, date2):
    """Find difference in months between 2 dates """

    try:
        return (date2.year - date1.year)*12 + (date2.month - date1.month)
    except Exception as err:
        print err+" .The argument was {}.".format(date2)
        return np.NaN

# Used in read result once - can be combined
def diff_month_date(date_in):
    return (date_in.year - initDate.year)*12 + date_in.month - initDate.month

def add_months(months):
    """
    Returns the date for several months after the initDate.
    """
    month = initDate.month - 1 + months
    year = int(initDate.year + month / 12)
    month = month % 12 + 1
    return date(year, month, monthrange(year, month)[1])  # Reporting on the last day of the month

##  pp_helpers - Not used
def add_months(sourcedatestr, months):
    """Find and add report dates between a date and number of months given."""
    if isinstance(sourcedatestr, str):
        sourcedate = datetime.strptime(sourcedatestr, '%Y-%m-%d').date()
    else:
        sourcedate = sourcedatestr

    length_months = len(months)
    month = [sourcedate.month - 1 + months[i] for i in xrange(length_months)]
    year = [int(sourcedate.year + x / 12) for x in month]
    month = [month[i] % 12 + 1 for i in xrange(length_months)]

    # get the end of month day number date
    #length_year = len(year)
    day = [min(sourcedate.day, calendar.monthrange(year[i], month[i])[1])
           for i in xrange(length_months)]
    day = [calendar.monthrange(year[i], month[i])[1]
           for i in xrange(length_months)]
    dates = [datetime(year[i], month[i], day[i]).date()
             for i in xrange(length_months)]

    return dates, day

def getLength(oil):
    """
    Calculates the length of oil time series.
    Here, the last data point of the time series is the last non-zero production month.
    """
    N = len(oil)
    lens = [x.shape[0] for x in oil]
    for i in range(N):
        while oil[i][lens[i]-1] == 0:
            lens[i] -= 1
            if lens[i] == 0:
                break
    return lens

def check_lens(items):
    """
    Return False if all of them have the same length.
    """
    lens = set([len(x.split(',')) for x in items[1:]])
    if len(lens) == 1:
        return False
    else:
        print items[0]
        return True