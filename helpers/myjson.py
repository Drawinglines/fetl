"""
The goal is to make strings ASCII.
Code from http://stackoverflow.com/a/33571117
"""
import sys
import json



def json_load_byteified(file_handle):
    try:
        raw_results = json.load(file_handle, object_hook=_byteify)
    except ValueError:
        print "Syntax Error in the config file."
        sys.exit(1)
    return _byteify(raw_results, ignore_dicts=True)


# Depricate this??
def json_loads_byteified(json_text):
    try:
        raw_results = json.loads(json_text, object_hook=_byteify)
    except ValueError:
        print "Syntax Error in the config file."
        sys.exit(1)
    return _byteify(raw_results, ignore_dicts=True)

def _byteify(data, ignore_dicts=False):
    # if this is a unicode string, return its string representation
    if isinstance(data, unicode):
        return data.encode('utf-8')
    # if this is a list of values, return list of byteified values
    if isinstance(data, list):
        return [_byteify(item, ignore_dicts=True) for item in data]
    # if this is a dictionary, return dictionary of byteified keys and values
    # but only if we haven't already byteified it
    if isinstance(data, dict) and not ignore_dicts:
        return {
            _byteify(key, ignore_dicts=True): _byteify(value, ignore_dicts=True)
            for key, value in data.iteritems()
        }
    # if it's anything else, return it in its original form
    return data

def parse_config(config_name):
    with open(config_name, 'rt') as conf:
        config = json_load_byteified(conf)
    config['oil']['kernel']['kps']['params'] = [config['oil']['kernel']['kps']['params']['c'], \
                                                config['oil']['kernel']['kps']['params']['d']]
    config['water']['kernel']['kps']['params'] = [config['water']['kernel']['kps']['params']['c'], \
                                                config['water']['kernel']['kps']['params']['d']]
    config['gas']['kernel']['kps']['params'] = [config['gas']['kernel']['kps']['params']['c'], \
                                                config['gas']['kernel']['kps']['params']['d']]
    return config


def update_kps(setting, target, best_param):

    setting[target]['kernel']['kps']['params'][0] = best_param['params'][0]
    setting[target]['kernel']['kps']['params'][1] = best_param['params'][1]
    setting[target]['kernel']['kps']['sigma'] = best_param['sigma']

    return setting

def write_kps_to_config(config_name, config):
    with open(config_name, 'w') as f:
        json.dump(config, f, ensure_ascii=False, indent=4, sort_keys=True)

