




def encode_pools(names):
    uni = list(set(names))
    name_map = dict(zip(uni, xrange(len(uni))))
    return [name_map[x] for x in names]