# -*- coding: utf-8 -*-
"""
Created on Wed Aug 03 12:34:23 2016
Python batch master
@author: Administrator
"""

import json
import boto3
import botocore
import sys

sys.path.insert(0, '..\FFR')
import main_ffr

FILENAME = sys.argv[1]  # "config_ffr.json"

dynamodb = boto3.resource('dynamodb', region_name='us-west-2', endpoint_url="https://dynamodb.us-west-2.amazonaws.com")

# Check if table is active else create a new table
try:
    table = dynamodb.Table('FFR_Hyper_parameters')
    print "Table status:", table.table_status

except botocore.exceptions.ClientError:
    print "Creating table..."
    table = dynamodb.create_table(
        TableName='FFR_Hyper_parameters',
        KeySchema=[
            {'AttributeName': 'Run_Id', 'KeyType': 'HASH'},  # Partition key
            {'AttributeName': 'Algorithm', 'KeyType': 'RANGE'}  # Sort key
        ],
        AttributeDefinitions=[
            {'AttributeName': 'Run_Id', 'AttributeType': 'N'},
            {'AttributeName': 'Algorithm', 'AttributeType': 'S'},
        ],
        ProvisionedThroughput={'ReadCapacityUnits': 10, 'WriteCapacityUnits': 10}
    )
    print "Table status:", table.table_status

# Reads the name of files in an s3 bucket
# s3 = boto3.resource('s3')
# bucket = s3.Bucket('airflowtest')
# for obj in bucket.objects.all():
#    print obj.key

# Download file from s3
s3 = boto3.client('s3')
list_obj = s3.list_objects(Bucket='airflowtest')['Contents']
print list_obj
for key in list_obj:

    if key['Key'] == FILENAME:
        s3.download_file('airflowtest', key['Key'], key['Key'])

file_handle = open(FILENAME, "r")
params = json.load(file_handle)
# print params
# print

response = table.put_item(Item=params)

# Read items
try:
    response = table.get_item(
        Key={'Run_Id': 1, 'Algorithm': 'FFR'}
    )
except botocore.exceptions.ClientError as e:
    print(e.response['Error']['Message'])
else:
    item = response['Item']
    print("GetItem succeeded:")
    print item

    # if params["script_name"] == "main_ffr":
    #    main_ffr.main_ffr(params["target"], params["state"], params["source"],
    #                      params["features"], params["length"], params["table_name"])