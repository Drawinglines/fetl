



def Check_Valid_Producer(data, time_trsh, prod_trsh, diff_days, month_end):

    """
    this function checks whether the well is a valid producer for forecasting and gives
    # the index of when the well gone offline by checking the catastrophic failure criterias
    """
    flag, last_prod_indx = None, 0

    if bool(data) is False:
        #no data available
        flag = 6
        return flag, last_prod_indx, 0

    #Check the days down for given data
    oil = np.array(data['oil_mo'], float)
    water = np.array(data['wtr_mo'], float)
    gas = np.array(data['gas_mo'],float)
    welldates = data['report_date_mo']
    prod_days = np.array(data['prod_days'], float)
    days_down, days_in_month = down_time(prod_days, month_end)

    #calculate effective oil & effective gas

    eff_oil = sum(i > prod_trsh["oil"] for i in oil)
    eff_gas = sum(i > prod_trsh["gas"] for i in gas)

    if eff_oil >= time_trsh["oil"]:
        flag = 0
    elif eff_gas >= time_trsh["gas"]:
        flag = 1
    elif np.sum(np.abs(oil)) != 0 or np.sum(np.abs(gas)) != 0:
        flag = 2
    elif np.sum(np.abs(oil)) == 0 or np.sum(prod_days) == 0: #never produced
        flag = 3
        #return flag, last_prod_indx, 0
    elif np.sum(np.abs(oil)) == 0 and np.sum(np.abs(water)) != 0: # its an injector
        flag = 4
        #return flag, last_prod_indx, 0
    #elif min(len(welldates), len(oil), len(gas)) < min(time_trsh["oil"], time_trsh["gas"]):
        #flag = 5
        #return flag, last_prod_indx, 0
    else:
        flag = 5
        #return flag, last_prod_indx, 0

    first_prod_indx = 0
    return flag, len(prod_days), first_prod_indx