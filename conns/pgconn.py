import psycopg2
from pkg_resources import resource_filename
import json
import os

def establish_db_conn(conn_num = "pg1"):
    """
    Establish DB connection with PostgreSQL database.
    Remember to close your connections once youre done.
    Args:
        database : String object. Name of database to connect to.

    Returns:
        conn : Database connection object
        cursor : Database connection object
    """

    print "Using {} DB settings".format(conn_num)

    # path = str(resource_filename('conns', 'config.json')).replace('\\', '/')
    path = os.path.join(os.path.dirname(__file__), 'config.json')

    print path

    try:
        with open(path, 'r') as conf:
            conn_config = json.load(conf)
            print "Loaded config locally"

    except Exception as err_msg:
        print err_msg
        print "Error - Please enter your credentials in config.json.\
        Follow steps from confluence Preprocessing page"

    print conn_config[conn_num]['database']

    conn = psycopg2.connect(database=conn_config[conn_num]['database'],
                            user=conn_config[conn_num]['user'],
                            password=conn_config[conn_num]['password'],
                            host=conn_config[conn_num]['host'],
                            port=conn_config[conn_num]['port'])
    cursor = conn.cursor()
    return conn, cursor

