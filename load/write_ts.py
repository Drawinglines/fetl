from conns.pgconn import establish_db_conn, write_DB
import csv
import tqdm

def write2pg(data, name, target):

    # Format Taha's D_L to Aamir's D_L
    if target in ['oil', 'water']:
        tgtpd = target + '_pd'
    else:
        tgtpd = target

    output = defaultdict(list)
    for item in tqdm(xrange(len(data['padID']))):
        tlen = len(data[target][item])
        output['API_prod'].append([data['padID'][item]]*tlen)
        output['prod_days'].append(data['proddays'][item])
        output['report_date_mo'].append(data['date'][item])
        output[tgtpd].append(data[target][item])
        output[tgtpd+'_fore_mean'].append(data[target+'_fore'][item])
        output[tgtpd+'_fore_std'].append(data[target+'_fore_std'][item])
        output[tgtpd+'_fore_p10'].append(data[target+'_fore10'][item])
        output[tgtpd+'_fore_p90'].append(data[target+'_fore90'][item])

    conn, cursor = establish_db_conn()
    write_DB(conn, cursor, output,
             verbosity=False, schema='results',
             table_name=name, drop_table=True)
    conn.close()



def write_DB(conn, cursor, results_data, verbosity, schema, table_name, drop_table=False):
    """
    Write the results to the PostgreSQL database. The column names will be same
    as the keys in your data. All the data for a well must be of the same length,
    even non-timeseries
    attributes need to be of the same length, otherwise this method will give an
    Index out of range error. This function accepts both formats -
    List of dicts & Dict of lists.

    Type constraints -
        API_prod values need to be long objects
        report_date_mo values need to be datetime.date objects

    Args:
        conn : Database connection object

        cursor : Database connection object

        results_data : Data to be written to the database.
                       Can be a List of dictionarys or Dictionary of Lists.

        verbosity : Boolean flag. If true function will print summary of func execution to console.

        schema : String object. Name of schema in which to create the table.

        table_name : String object. Name of the database table to write data

        drop_table : Boolean object. If true will drop table of same table name if it exists

    Returns:
        Nothing

    """

    #convert it to Taha's format if its not already
    if type(results_data) == list:
        results_data = ld_to_dl(results_data)

    #results_data.pop("accptflag", None)

    # Drop table if exists
    if drop_table:
        try:
            query = """DROP TABLE IF EXISTS {}."{}";""".format(schema, table_name)
            cursor.execute(query)
            conn.commit()
        except Exception as error:
            print error
            print "Error while dropping table"

    results_keys = sorted(results_data.keys())
    temp_b = type(datetime.strptime('1900-01-01', '%Y-%m-%d').date()) #type check

    #Generate type dictionary for keys
    nos_items = len(results_data[results_keys[0]])

    type_dict = []
    for key in results_keys:
        try:
            value_type = None
            for well_data in xrange(nos_items):
                for well_value in xrange(len(results_data[key][well_data])):

                    if results_data[key][well_data][well_value] != None:
                        value_type = type(results_data[key][well_data][well_value])
                        break
                    else:
                        continue
                    break

        except Exception as error:
            print error
            print key, results_data[key]

        #if key has upper case characters, attach quotes
        if any(s.isupper() for s in key):
            key = "\""+key+"\""

        if value_type == None:
            type_dict.append(key+' VARCHAR(995)')
        elif value_type == bool:
            type_dict.append(key+' BOOLEAN')
        elif value_type == long:
            type_dict.append(key+' BIGINT')
        elif value_type == temp_b:
            type_dict.append(key+' DATE')
        elif value_type == str:
            type_dict.append(key+' VARCHAR(255)')
        else:
            #a == float or a == int or a == np.int32  :
            type_dict.append(key+' numeric NULL')

    structure = ",".join(type_dict)
    query = """CREATE TABLE IF NOT EXISTS {}."{}" ({})""".format(schema, table_name, structure)
    try:
        cursor.execute(query)
        conn.commit()
    except Exception as error:
        print error

    # Insert the rows
    values = list()
    count = 0

    if "API_master" in results_data.keys():
        key = "API_master"
    elif "API_prod" in results_data.keys():
        key = "API_prod"
    else:
        key = results_keys[0]

    #Build tuples out of the values for batch insert
    try:
        for outer_indx in xrange(len(results_data[key])):
            for inner_indx in xrange(len(results_data[key][outer_indx])):
                row_val = []
                for key in results_keys:

                    if str(results_data[key][outer_indx][inner_indx]) == 'None':
                        row_val.append(None)
                    else:
                        row_val.append(str(results_data[key][outer_indx][inner_indx]))
                count += 1
                values.append(tuple(row_val))
    except Exception as error:

        print error, inner_indx, outer_indx
        print "Length of all the values to be written should be the same"

    del results_data

    #Batch insert using mogrify approach
    template = "("+",".join(["%s"]*len(results_keys))+")"
    values = tuple(values)
    step_size = 25000
    n_batch = int(np.ceil(count/step_size))
    ind = [min(x*step_size, count) for x in range(n_batch+1)]

    start_time = time()
    for temp_indx in xrange(n_batch):
        args_str = ','.join(cursor.mogrify(template, val) for val in values[ind[temp_indx]:ind[temp_indx+1]])
        try:
            query = """INSERT INTO {}."{}" VALUES {};""".format(schema, table_name, args_str)
            cursor.execute(query)
            #cursor.execute("INSERT INTO "+schema+"."+table_name+" VALUES " + args_str)
        except Exception as error:
            print error

    if verbosity:
        print "Number of rows : ", count
        print "Insert time - "+ str(round((time()-start_time), 2))+" secs"
        print "-"*59
        print
    conn.commit()
    del values
    return



def create_index(conn, cursor, verbosity, schema, table_name, column, index_name):
    """
    Creates index on the database table
    Args :
        conn : Database connection object

        cursor : Database connection object

        verbosity : Boolean flag. If true function will print summary of func execution to console.

        schema : String object. Name of schema in which the table is written.

        table_name : String object. Name of the database table on which is to be indexed

        column : String object. Name of Column on which index is to be created

        index_name : String object. Unique index name.
                     Hint : Use a combination of column name & time-stamp to make it unique
                     if you cant think of anything else

    Returns:
        Nothing
    """
    try:
        query = """CREATE INDEX "{}" ON "{}"."{}" USING btree\
                ("{}");""".format(index_name, schema, table_name, column)
        if verbosity: print "Creating Index..."
        cursor.execute(query)
        conn.commit()
        if verbosity: print "Index created."

    except Exception as error:
        print error
        print "Index creation unsuccessful"

    return


def write_csv(temp_a, temp_b):
    """write two lists to CSVs"""
    result_file = open("Worthy_APIs_new.csv", 'wb')
    wr_obj = csv.writer(result_file)
    for j in temp_a:
        wr_obj.writerow([j])
    result_file.close()

    result_file = open("Not_Worthy_APIs_new.csv", 'wb')
    wr_obj = csv.writer(result_file)
    for k in temp_b:
        wr_obj.writerow([k])
    result_file.close()


def write_data_csv(data):
    """Write data to csv to check moving avg outlier results"""
    result_file = open(str(data["API_prod"][0])+"_2.csv", 'wb')
    wr_obj = csv.writer(result_file)

    for i in range(len(data['report_date_mo'])):

        wr_obj.writerow([data['report_date_mo'][i], data["prod_days"][i],
                         data["oil_mo"][i], data["oil_mo_mod"][i]])

    result_file.close()