import psycopg2

from conns.pgconn import establish_db_conn


def opt_kernel_param_tracker(run_id, params, epoch, objective):
    # Establish the connection
    conn, cursor = establish_db_conn('pg1')
    # Table of params
    param_id = [[0, 1], ['c', 'd']]

    print objective
    print epoch
    print params
    print run_id

    for i in range(0, 2):
        for j in range(0, len(params['params'][i])):
            # print params['params'][i][j]
            try:
                cursor.execute("""INSERT INTO kernel.opt_tracking (run_id, param_id, param_number, param_index, param_value, epoch, objective) \
                                VALUES (%s, %s, %s, %s, %s, %s, %s);""", [run_id, param_id[1][i], j, 'null', params['params'][i][j], epoch, objective])
            except psycopg2.Error as e:
                print('Error with sql insert - {}').format(e)
                cursor.close()
                conn.close()
                sys.exit(1)

    for k in range(0, len(params['sigma'])):
        try:
            cursor.execute("""INSERT INTO kernel.opt_tracking (run_id, param_id, param_number, param_index, param_value, epoch, objective) \
                                VALUES (%s, %s, %s, %s, %s, %s, %s);""", [run_id, 'sigma', k, 'null', params['params'][i][j], epoch, objective])
        except psycopg2.Error as e:
            print('Error with sql insert - {}').format(e)
            cursor.close()
            conn.close()
            sys.exit(1)


    conn.commit()
    cursor.close()
    conn.close()