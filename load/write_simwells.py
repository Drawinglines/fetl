import


def write_sim_db(similars, setting):
    # Establish the connection
    conn = psycopg2.connect(database='terra', user=config['user'], password=config['password'], host=config['host'], port=config['port'])
    cursor = conn.cursor()
    query = """CREATE TABLE similar_wells.{} (target_well BIGINT, training_well BIGINT,
     seq_date DATE, similarity_score DOUBLE PRECISION, winp INT, winq INT);""".format(setting['table_name'])
    cursor.execute(query)

    for sim in tqdm(similars):
        try:
            query = """INSERT INTO similar_wells.{} VALUES {}""".format(setting['table_name'], sim)
            cursor.execute(query)
        except:
            pass

    conn.commit()
    cursor.close()
    conn.close()