import psycopg2
import sys
from json import dumps

from conns.pgconn import establish_db_conn


def tabt_fore_run_init(run_id, old_config):
    # Establish the connection
    conn, cursor = establish_db_conn('pg1')
    # Make sure json is ASCII for writing (replace single with double quotes)
    old_config = dumps(old_config, ensure_ascii=False)
    try:
        cursor.execute("""INSERT INTO tabt.fore_run (run_id, table_name, run_time_start, run_time_fin, config_file_old,\
            config_file_new, enr_status, enr_ts_tbl_name, enr_m_tbl_name, enr_time, eval_status,\
            eval_ts_tbl_name, eval_m_tbl_name, eval_time) VALUES (%s , null, now(), null, %s,\
            null, FALSE, null, null, null, null, null, null, null);"""\
            ,[run_id, old_config])
    except psycopg2.Error as e:
        print('Error with sql insert - {}').format(e)
        cursor.close()
        conn.close()
        sys.exit(1)

    conn.commit()
    cursor.close()
    conn.close()


def tabt_fore_run_update(table_name, run_id, new_config):
    # Establish the connection
    conn, cursor = establish_db_conn('pg1')
    # Make sure json is ASCII for writing (replace single with double quotes)
    new_config = dumps(new_config, ensure_ascii=False)

    try:
        cursor.execute("""UPDATE tabt.fore_run set table_name = %s, config_file_new = %s WHERE run_id = %s;"""\
            ,[table_name, new_config, run_id])
    except psycopg2.Error as e:
        print('Error with sql insert - {}').format(e)
        cursor.close()
        conn.close()
        sys.exit(1)

    conn.commit()
    cursor.close()
    conn.close()


def tabt_fore_run_fin(run_id):
    # Establish the connection
    conn, cursor = establish_db_conn('pg1')
    try:
        cursor.execute("""UPDATE tabt.fore_run set run_time_fin = now() WHERE run_id = %s;""" \
                       , [run_id])
    except psycopg2.Error as e:
        print('Error with sql insert - {}').format(e)
        cursor.close()
        conn.close()
        sys.exit(1)

    conn.commit()
    cursor.close()
    conn.close()

tabt_fore_run_fin('113')





# def write_tabt_runfin(best_param, setting):
#     # Establish the connection
#     conn, cursor = establish_db_conn('pg1')
#     query = """create table kernel.test (test int)"""
#     cursor.execute(query)
#
#     conn.commit()
#     cursor.close()
#     conn.close()

