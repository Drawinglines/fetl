


def down_time(prod_days, days_month=[], report_date_mo=[]):
    """This function calculates the number of days well
    has been down in each month. This function needs either report_date_mo
    or days_month NOT BOTH. Pass an empty list

    Args :
        report_date_mo : a list of production report dates.
                        The dates are datetime.date objects

        prod_days : a list of integers showing number of days the well produced

        days_month : a list of integers which give the number of days in that month

    Returns:
        days_down : a list of integers containing difference between total days of
                    the month & prod_days of the month

        days_month : a list of integers which give the number of days in that month
    """
    if len(report_date_mo) == 0 and len(days_month) == 0:
        print "Error. Check down_time function's parameters passed"
        sys.exit(0)

    if len(days_month) == 0:
        for date_val in report_date_mo:
            #array of Dates extract Month and Year from the string and
            #find the number of days in that month
            days_month.append(monthrange(date_val.year, date_val.month)[1])

    #days_down= (a-b for a,b zip(days_month,prod_days))
    days_down = map(sub, days_month, prod_days)
    return days_down, days_month


def get_oilprices(cursor, schema="public_data", tablename="oil_price_monthly",
                  ref_dates=None, input_oilprice_dict=None, API=None):
    """Get oil prices and sync it with the report dates for the current well"""

    if input_oilprice_dict is None:
        cursor.execute("SELECT * FROM "+schema+"."+tablename+";")
        row = cursor.fetchall()
        oil_prices = [row[i][1] for i in xrange(len(row))]

        oilprice_dates = [row[i][0] for i in xrange(len(row))]

    else:
        oil_prices = input_oilprice_dict['oilprice_value']
        oilprice_dates = input_oilprice_dict['oilprice_date']
    #del input_oilprice_dict


    if ref_dates is not None: # given set of dates for which oil priuce is needed
        ref_dates_obj = ref_dates
    else:
        ref_dates_obj = oilprice_dates

    #==== get the index of requested dates compared to ref dates in database
    istart = diff_month(oilprice_dates[0], ref_dates_obj[0])
    indx = range(istart, istart+len(ref_dates_obj))

    price_date = []
    price_val = []
    try:
        for val in indx:
            price_date.append(oilprice_dates[val])
            price_val.append(oil_prices[val])
    except Exception as error:
        print error
        print "Price length error", API

    oilprice_dict = defaultdict(list)
    oilprice_dict['oilprice_value'] = price_val
    oilprice_dict['oilprice_date'] = price_date

    return oilprice_dict
