

from __future__ import division

from collections import defaultdict
from datetime import datetime, date
import numpy as np
from tqdm import tqdm   # For progress bar
import psycopg2

# Custom Packages
from helpers.gen import mynum
from helpers.dels import *
from conns.pgconn import establish_db_conn

"""
    Depricated not tested
"""
def get_well_ids(cursor, column_names, state_api, schema="public_data",
                 db_prodtable_name="bigprod_ts"):
    """
    Gets the well APIs from the table data is being retrieved.
    Use the function describe_table() to get column_names & structure parameters.

    Args:
        cursor : Database connection object
        column_names : list of column names of table
        state_api : State whose APIs need to be retrieved. Pass None to
                    read in all the values of the table.
        schema : Schema where table is stored
        db_prodtable_name : Name of database table

    Returns:
       well_apis : A sorted list of well APIs

    """
    if "API_master" in column_names:
        key = "API_master"
    elif "API_prod" in column_names:
        key = "API_prod"
    else:
        key = None
        print "Looking for 'API_master' or 'API_prod' column name. Column not found"
        return

    #try:
    if state_api is None:
        query = """SELECT distinct("{}") FROM {}."{}";""" .format(key, schema,
                                                                  db_prodtable_name)
    else:
        query = """SELECT distinct("{}") FROM {}."{}" WHERE\
                   "state_API"={};""".format(key, schema,
                                             db_prodtable_name, state_api)

    cursor.execute(query)
    row = cursor.fetchall()

    #remove None elements from list
    unique_pids = set([int(v[0]) for v in row if v[0] != None])
    return sorted(list(unique_pids))
    #except Exception as error:
        #print error


def describe_table(cursor, schema, db_prodtable_name):

    """Get column names from table.

    Args:
        cursor : Database connection object
        schema : Schema of the database table
        db_prodtable_name : Name of database table

    Returns:
        column_names : A list containing column names of the table
        structure : A string that is needed as part of SQL queries to
                    retrieve the columns
    """
    cursor.execute("""Select column_name, data_type from INFORMATION_SCHEMA.COLUMNS\
                   where table_schema = '{}' and table_name = '{}';""" .format(schema, db_prodtable_name))
    results = cursor.fetchall()
    column_names = [row[0] for row in results]

    #append quotes to column names having upper case letters
    full_names = map(lambda x: "\""+x+"\"" if any(s.isupper() for s in x) else x, column_names)
    structure = ','.join(full_names)

    return column_names, structure


def get_well_data(cursor, structure, column_names, api_prod,
                  schema, db_prodtable_name,
                  start_date='1900-01-01', end_date=_NOW_DATE_STR):

    """Gets production data from the database for the given well API.
       Use the function describe_table() to get column_names & structure parameters.

    Args:
        cursor : Database connection object
        column_names : A list of column names of the table
        structure : A string that is needed as part of SQL queries to
                    retrieve the columns
        api_prod : API of the well whose data you want
        schema : Schema where table is stored
        db_prodtable_name : Name of database table
        start_date : Start date of production records. Default is '1900-01-01'
        end_date : End date of production records. Default is today's date.

    Returns:
        data : A dictionary of lists, containing production data of the
                given table with its column names as keys.
    """

#    if type(api_prod) == list:
#        query = "SELECT "+structure+" FROM "+schema+"."+db_prodtable_name+
#                   " WHERE Api_prod in ( " + ",".join(map(str,api_prod)) + " ) \
#        and report_date>%s and report_date<=%s order by Api_prod , report_date ASC limit 10;"
#        cursor.execute(query,[start_date,end_date])
#    else:
    query = """SELECT {} FROM {}."{}" WHERE "API_prod"={}\
    and report_date_mo>'{}' and report_date_mo<='{}' order by "API_prod",\
    report_date_mo ASC;""".format(structure, schema, db_prodtable_name,
                                  api_prod, start_date, end_date)

    cursor.execute(query)

    row = cursor.fetchall()
    data = defaultdict(list)
    for item in row:
        item = np.array(map(lambda x: np.nan if x is None else x, item))

        for j, key in enumerate(column_names):
            data[key].append(item[j])

    if "water_mo" in data.keys():
        data["wtr_mo"] = data.pop("water_mo")

    return data


def get_well_data_ts(cursor, structure, column_names, api_prod, schema,
                     db_prodtable_name,
                     start_date='1900-01-01', end_date=_NOW_DATE_STR):
    """
    Get data from a table in time series format which results in faster read.
    Use the function describe_table() to get column_names & structure parameters.

    Args:
        cursor : Database connection object
        column_names : A list of column names of the table
        structure : A string that is needed as part of SQL queries to
                    retrieve the columns
        api_prod : API of the well whose data you want
        schema : Schema where table is stored
        db_prodtable_name : Name of database table
        start_date : Start date of production records. Default is '1900-01-01'
        end_date : End date of production records. Default is today's date.

    Returns:
        data : A dictionary of lists, containing production data of the
                given table with its column names as keys.
    """

    conversion = {"oil_mo":"float", "wtr_mo":"float", "gas_mo":"float",
                  "prod_days":"float", "wor":"float", "cumoil":"float",
                  "cumwat":"float", "fw":"float", "fo":"float", "price":"float"}

    #handle other columns with _ts in the column name. Let their type be string.
    #done to speed up the process of adding new attributes
    extra_cols = [col for col in column_names if "_ts" in col]
    for col in extra_cols:
        conversion[col] = "str"

    query = """SELECT {} FROM {}."{}" WHERE "API_prod"=\
            {};""".format(structure, schema, db_prodtable_name, api_prod)
    cursor.execute(query)

    row = list(cursor.fetchall())
    row[0] = list(row[0])
    data = defaultdict(list)

    indx = column_names.index("report_date_mo")
    data['report_date_mo'] = [datetime.strptime(x, "%Y-%m-%d").date()
                              for x in row[0][indx].split(',')]

    #====Handle cases when the column has a null value. Replace by a string of 0
    length = len(data['report_date_mo'])

    for j, key in enumerate(column_names):

        if key == "report_date_mo":
            continue

        if key in conversion.keys():
            if row[0][j] is None:
                data[key] = [0.0]*length
            else:
                if conversion[key] == "float":
                    data[key] = [abs(float(x)) if x != None else 0 for x in row[0][j].split(',')]

                else:
                    #if string to handle extra columns
                    data[key.replace("_ts","")] = [x for x in row[0][j].split(',')]
        else:
            data[key] = [row[0][j]]*length

    sd_obj = datetime.strptime(start_date, '%Y-%m-%d').date()
    ed_obj = datetime.strptime(end_date, '%Y-%m-%d').date()

    keys = data.keys() #just use column_names instead

    for i, val in enumerate(data['report_date_mo']):

        if  sd_obj > val or val > ed_obj:
            for k in keys:
                data[k].pop(i)

    if "water_mo" in data.keys():
        data["wtr_mo"] = data.pop("water_mo")

    return data

def get_multi_well_data(cursor, schema, db_table_name, return_format="L_of_D", state_api=None, well_ids=[]):

    """
    Wrapper around the other read methods, to read multiple wells at once
    instead of reading only a single well at a time.

    Args:
        cursor : Database connection object

        schema : Schema where table is stored

        db_table_name : Name of database table

        return_format : String object. Format to return data, list of dictionary
                        OR dictionary of lists. Values can be - 'L_of_D' or 'D_of_L'.

        state_api : Integer object. State whose wells you want

        well_ids : List of Integer objects. Provide specific wells ids to be processed,
                   instead of whole state. In this case state_api value will be disregarded.

   Returns:
       data : Data for multiple wells from the given table in the specified format.
    """

    #make sure parameters are correct
    if state_api==None and len(well_ids) == 0:
        print "Error. get_multi_well_data() needs either state_api or well_ids to be specified"
        return

    column_names, structure = describe_table(cursor, schema, db_table_name)

    if len(well_ids) == 0:
        well_ids = get_well_ids(cursor, column_names, state_api, schema, db_table_name)

    results = []

    if "_master" in db_table_name:
        for well in well_ids:
            static_data = get_static_data(cursor, structure, column_names,
                                      well, schema,
                                      db_table_name)
            results.append(static_data)
    elif "_ts" in db_table_name:
        for well in well_ids:
            prod_data = get_well_data_ts(cursor, structure, column_names, well,
                                schema, db_table_name)
            results.append(prod_data)
    else:
        for well in well_ids:
            prod_data = get_well_data(cursor, structure, column_names, well,
                                schema, db_table_name)
            results.append(prod_data)

    if return_format == "D_of_L":
        results = ld_to_dl(results)
        return



def get_static_data(cursor, static_structure, static_column_names, api_prod,
                    schema="public_data",
                    db_mastertable_name="big_master_detail"):

    """Getting the master (static) data from DB
       Use the function describe_table() to get column_names & structure parameters.

      Args:
        cursor : Database connection object
        structure : A string that is needed as part of SQL queries to
                    retrieve the columns
        api_prod : API of the well whose data you want
        schema : Schema where table is stored
        db_prodtable_name : Name of database table

    Returns:
        static_data : static attributes about a well.
                      Check "Static attributes dictionary" confluence page
                      for a description of the attributes.

    """

    query = """SELECT {} FROM {}."{}" WHERE "API_master"\
            = {};""".format(static_structure, schema, db_mastertable_name, api_prod)
    cursor.execute(query)
    row = cursor.fetchall()

    static_data = defaultdict(list)

    if row != []:
        for i, key in enumerate(static_column_names):

            if bool(row[0][i]):
                static_data[key].append(row[0][i])
            else:
                static_data[key].append(None)
    else:
        #if no data is available return an empty dict
        for i, key in enumerate(static_column_names):
            static_data[key].append(None)

        static_data["API_master"] = [api_prod]

    return static_data


def read_from_db(train_query, test_query):
    """
    This function reads the data from the database and converts them to a defaultdict format.
    It also performs some mild preprocessing on the data such as convertion of the dates to diff
    months and handling missing data with handle_missing() function.
    """
    # Establish the connection
    conn = psycopg2.connect(database='terra', user=config['user'], password=config['password'],\
                            host=config['host'], port=config['port'])
    cursor = conn.cursor()
    cursor.execute(train_query)
    row = cursor.fetchall()
    data = defaultdict(list)
    print "Reading from the database:"
    for item in tqdm(row):
        # "API_prod", report_date_mo, oil_pd, water_pd, gas_pd, prod_days_up, lat, longi, depth, pool_name
        if check_lens([item[x] for x in [0, 1, 2, 3, 4, 5]]):
            continue
        oil = np.array([mynum(float, x) for x in item[2].split(',')])
        if oil.sum() == 0:
            continue
        dates = np.array([diff_month(x) for x in  item[1].split(',')])
        index = np.argsort(dates)
        dates = dates[index]
        water = np.array([mynum(float, x) for x in item[3].split(',')])[index]
        gas = np.array([mynum(float, x) for x in item[4].split(',')])[index]
        days = np.array([mynum(int, x) for x in item[5].split(',')])[index]
        dates, oil, water, gas, days = handle_missing(dates, oil[index], water, gas, days)

        data['padID'].append(item[0])
        data['dates'].append(dates)
        data['oil'].append(oil)
        data['water'].append(water)
        data['gas'].append(gas)
        data['proddays'].append(days)
        if (item[6] is None) or (item[7] is None):
            data['location'].append(np.array([0, 0]))
        else:
            data['location'].append(np.array([mynum(float, item[5]), mynum(float, item[6])]))
        if item[8] is None:
            data['depth'].append(0)
        else:
            data['depth'].append(mynum(float, item[7]))
        data['pool'].append(item[9])

    # Coding the pool names into numerical categories
    data['pool'] = encode_pools(data['pool'])

    if test_query is None:
        cursor.close()
        conn.close()
        return data, None

    cursor.execute(test_query)
    row = cursor.fetchall()
    indexes, test_dates, counter = list(), list(), 0
    for x in row:
        if x[0] in data['padID']:
            indexes.append(data['padID'].index(x[0]))
            test_dates.append(diff_month(str(x[1])))
        else:
            counter += 1
    print "{} out of {} test wells are not found in the training".format(counter, len(row))

    cursor.close()
    conn.close()
    return data, zip(indexes, test_dates)