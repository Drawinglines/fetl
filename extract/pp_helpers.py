# -*- coding: utf-8 -*-
"""
Created on Fri May 06 13:41:36 2016

@author: Aamir
"""

from __future__ import division
import sys
from collections import defaultdict
from datetime import datetime
import calendar
from calendar import monthrange
from time import strftime, time
from operator import sub
import csv
import json
from pkg_resources import resource_filename
import psycopg2
import numpy as np


_NOW_DATE_STR =  str(strftime("%Y-%m-%d")) #"2015-06-01"
_INIT_DATE = datetime.strptime('1900-01-01', '%Y-%m-%d')
EPS = 0.001

def establish_db_conn(database="terra"):
    """
    Establish DB connection with PostgreSQL database.
    Remember to close your connections once youre done.
    Args:
        database : String object. Name of database to connect to.

    Returns:
        conn : Database connection object
        cursor : Database connection object
    """
    path = str(resource_filename('terraai_preprocessing.preprocessing',
                                 'config.json.json')).replace('\\', '/')

    try:
        with open(path, 'r') as conf:
            config = json.load(conf)

    except Exception as err_msg:
        print err_msg
        print "Error - Please enter your credentials in config.json.json.\
        Follow steps from confluence Preprocessing page"

    conn = psycopg2.connect(database=database, user=config['user'],
                            password=config['password'],
                            host=config['host'], port=config['port'])
    cursor = conn.cursor()
    return conn, cursor

def get_well_ids(cursor, column_names, state_api, schema="public_data",
                 db_prodtable_name="bigprod_ts"):
    """
    Gets the well APIs from the table data is being retrieved.
    Use the function describe_table() to get column_names & structure parameters.

    Args:
        cursor : Database connection object
        column_names : list of column names of table
        state_api : State whose APIs need to be retrieved. Pass None to
                    read in all the values of the table.
        schema : Schema where table is stored
        db_prodtable_name : Name of database table

    Returns:
       well_apis : A sorted list of well APIs

    """
    if "API_master" in column_names:
        key = "API_master"
    elif "API_prod" in column_names:
        key = "API_prod"
    else:
        key = None
        print "Looking for 'API_master' or 'API_prod' column name. Column not found"
        return

    #try:
    if state_api is None:
        query = """SELECT distinct("{}") FROM {}."{}";""" .format(key, schema,
                                                                  db_prodtable_name)
    else:
        query = """SELECT distinct("{}") FROM {}."{}" WHERE\
                   "state_API"={};""".format(key, schema,
                                             db_prodtable_name, state_api)

    cursor.execute(query)
    row = cursor.fetchall()

    #remove None elements from list
    unique_pids = set([int(v[0]) for v in row if v[0] != None])
    return sorted(list(unique_pids))
    #except Exception as error:
        #print error

def describe_table(cursor, schema, db_prodtable_name):

    """Get column names from table.

    Args:
        cursor : Database connection object
        schema : Schema of the database table
        db_prodtable_name : Name of database table

    Returns:
        column_names : A list containing column names of the table
        structure : A string that is needed as part of SQL queries to
                    retrieve the columns
    """
    cursor.execute("""Select column_name, data_type from INFORMATION_SCHEMA.COLUMNS\
                   where table_schema = '{}' and table_name = '{}';""" .format(schema, db_prodtable_name))
    results = cursor.fetchall()
    column_names = [row[0] for row in results]

    #append quotes to column names having upper case letters
    full_names = map(lambda x: "\""+x+"\"" if any(s.isupper() for s in x) else x, column_names)
    structure = ','.join(full_names)

    return column_names, structure


def get_well_data(cursor, structure, column_names, api_prod,
                  schema, db_prodtable_name,
                  start_date='1900-01-01', end_date=_NOW_DATE_STR):

    """Gets production data from the database for the given well API.
       Use the function describe_table() to get column_names & structure parameters.

    Args:
        cursor : Database connection object
        column_names : A list of column names of the table
        structure : A string that is needed as part of SQL queries to
                    retrieve the columns
        api_prod : API of the well whose data you want
        schema : Schema where table is stored
        db_prodtable_name : Name of database table
        start_date : Start date of production records. Default is '1900-01-01'
        end_date : End date of production records. Default is today's date.

    Returns:
        data : A dictionary of lists, containing production data of the
                given table with its column names as keys.
    """

#    if type(api_prod) == list:
#        query = "SELECT "+structure+" FROM "+schema+"."+db_prodtable_name+
#                   " WHERE Api_prod in ( " + ",".join(map(str,api_prod)) + " ) \
#        and report_date>%s and report_date<=%s order by Api_prod , report_date ASC limit 10;"
#        cursor.execute(query,[start_date,end_date])
#    else:
    query = """SELECT {} FROM {}."{}" WHERE "API_prod"={}\
    and report_date_mo>'{}' and report_date_mo<='{}' order by "API_prod",\
    report_date_mo ASC;""".format(structure, schema, db_prodtable_name,
                                  api_prod, start_date, end_date)

    cursor.execute(query)

    row = cursor.fetchall()
    data = defaultdict(list)
    for item in row:
        item = np.array(map(lambda x: np.nan if x is None else x, item))

        for j, key in enumerate(column_names):
            data[key].append(item[j])

    if "water_mo" in data.keys():
        data["wtr_mo"] = data.pop("water_mo")

    return data


def get_well_data_ts(cursor, structure, column_names, api_prod, schema,
                     db_prodtable_name,
                     start_date='1900-01-01', end_date=_NOW_DATE_STR):
    """
    Get data from a table in time series format which results in faster read.
    Use the function describe_table() to get column_names & structure parameters.

    Args:
        cursor : Database connection object
        column_names : A list of column names of the table
        structure : A string that is needed as part of SQL queries to
                    retrieve the columns
        api_prod : API of the well whose data you want
        schema : Schema where table is stored
        db_prodtable_name : Name of database table
        start_date : Start date of production records. Default is '1900-01-01'
        end_date : End date of production records. Default is today's date.

    Returns:
        data : A dictionary of lists, containing production data of the
                given table with its column names as keys.
    """

    conversion = {"oil_mo":"float", "wtr_mo":"float", "gas_mo":"float",
                  "prod_days":"float", "wor":"float", "cumoil":"float",
                  "cumwat":"float", "fw":"float", "fo":"float", "price":"float"}

    #handle other columns with _ts in the column name. Let their type be string.
    #done to speed up the process of adding new attributes
    extra_cols = [col for col in column_names if "_ts" in col]
    for col in extra_cols:
        conversion[col] = "str"

    query = """SELECT {} FROM {}."{}" WHERE "API_prod"=\
            {};""".format(structure, schema, db_prodtable_name, api_prod)
    cursor.execute(query)

    row = list(cursor.fetchall())
    row[0] = list(row[0])
    data = defaultdict(list)

    indx = column_names.index("report_date_mo")
    data['report_date_mo'] = [datetime.strptime(x, "%Y-%m-%d").date()
                              for x in row[0][indx].split(',')]

    #====Handle cases when the column has a null value. Replace by a string of 0
    length = len(data['report_date_mo'])

    for j, key in enumerate(column_names):

        if key == "report_date_mo":
            continue

        if key in conversion.keys():
            if row[0][j] is None:
                data[key] = [0.0]*length
            else:
                if conversion[key] == "float":
                    data[key] = [abs(float(x)) if x != None else 0 for x in row[0][j].split(',')]

                else:
                    #if string to handle extra columns
                    data[key.replace("_ts","")] = [x for x in row[0][j].split(',')]
        else:
            data[key] = [row[0][j]]*length

    sd_obj = datetime.strptime(start_date, '%Y-%m-%d').date()
    ed_obj = datetime.strptime(end_date, '%Y-%m-%d').date()

    keys = data.keys() #just use column_names instead

    for i, val in enumerate(data['report_date_mo']):

        if  sd_obj > val or val > ed_obj:
            for k in keys:
                data[k].pop(i)

    if "water_mo" in data.keys():
        data["wtr_mo"] = data.pop("water_mo")

    return data

def get_multi_well_data(cursor, schema, db_table_name, return_format="L_of_D", state_api=None, well_ids=[]):

    """
    Wrapper around the other read methods, to read multiple wells at once
    instead of reading only a single well at a time.

    Args:
        cursor : Database connection object

        schema : Schema where table is stored

        db_table_name : Name of database table

        return_format : String object. Format to return data, list of dictionary
                        OR dictionary of lists. Values can be - 'L_of_D' or 'D_of_L'.

        state_api : Integer object. State whose wells you want

        well_ids : List of Integer objects. Provide specific wells ids to be processed,
                   instead of whole state. In this case state_api value will be disregarded.

   Returns:
       data : Data for multiple wells from the given table in the specified format.
    """

    #make sure parameters are correct
    if state_api==None and len(well_ids) == 0:
        print "Error. get_multi_well_data() needs either state_api or well_ids to be specified"
        return

    column_names, structure = describe_table(cursor, schema, db_table_name)

    if len(well_ids) == 0:
        well_ids = get_well_ids(cursor, column_names, state_api, schema, db_table_name)

    results = []

    if "_master" in db_table_name:
        for well in well_ids:
            static_data = get_static_data(cursor, structure, column_names,
                                      well, schema,
                                      db_table_name)
            results.append(static_data)
    elif "_ts" in db_table_name:
        for well in well_ids:
            prod_data = get_well_data_ts(cursor, structure, column_names, well,
                                schema, db_table_name)
            results.append(prod_data)
    else:
        for well in well_ids:
            prod_data = get_well_data(cursor, structure, column_names, well,
                                schema, db_table_name)
            results.append(prod_data)

    if return_format == "D_of_L":
        results = ld_to_dl(results)
        return


def get_static_data(cursor, static_structure, static_column_names, api_prod,
                    schema="public_data",
                    db_mastertable_name="big_master_detail"):

    """Getting the master (static) data from DB
       Use the function describe_table() to get column_names & structure parameters.

      Args:
        cursor : Database connection object
        structure : A string that is needed as part of SQL queries to
                    retrieve the columns
        api_prod : API of the well whose data you want
        schema : Schema where table is stored
        db_prodtable_name : Name of database table

    Returns:
        static_data : static attributes about a well.
                      Check "Static attributes dictionary" confluence page
                      for a description of the attributes.

    """

    query = """SELECT {} FROM {}."{}" WHERE "API_master"\
            = {};""".format(static_structure, schema, db_mastertable_name, api_prod)
    cursor.execute(query)
    row = cursor.fetchall()

    static_data = defaultdict(list)

    if row != []:
        for i, key in enumerate(static_column_names):

            if bool(row[0][i]):
                static_data[key].append(row[0][i])
            else:
                static_data[key].append(None)
    else:
        #if no data is available return an empty dict
        for i, key in enumerate(static_column_names):
            static_data[key].append(None)

        static_data["API_master"] = [api_prod]

    return static_data


def process_data(cursor, data, time_trsh, prod_trsh, diff_days, oil_prices_full, flag_dict):
    """
    Impute data, check if valid producer, get data from combinatorics.
    """
#    flag_dict = {0:"Fully producing well", 1:"No oil but significant gas",
#                 2:"No significant oil or gas", 3:"Injector well",
#                 4:"Inconsistent well with the logic", 5:"No data available",
#                 6:"Produced less then threshold", 7:"Well abandoned"}

    if bool(data):
        #impute new data if some months are missing and add the corresponding dates
        data, month_end = impute_missing_data(data)

    data = calc_pd(data, month_end)

    #Check if it is a valid producer for forecasting
    accpt_flag, last_prod_indx, first_prod_indx = Check_Valid_Producer(data, time_trsh, prod_trsh,
                                                                      diff_days, month_end)
    if accpt_flag > 2 and accpt_flag != 5:
        data["accpt_flag"] = accpt_flag
        data["reason"] = flag_dict[accpt_flag]
        return data

    trim_param = "oil_mo" if accpt_flag==0 or accpt_flag==5 else "gas_mo"
    data, last_prod_indx, first_prod_indx = trim_dictionary(data,trim_param)
                                                            #last_prod_indx, first_prod_indx)


    ###accptflag,last_prod_indx,first_prod_indx = Check_Valid_Producer(data,
                                                #time_trsh,diff_days) #to check on trim data
    if last_prod_indx - first_prod_indx < time_trsh[trim_param.replace("_mo","")]:
        #if well has been a producer less this number of months only
        accpt_flag = 7
        #return 0

    data["accpt_flag"] = accpt_flag
    data["reason"] = flag_dict[accpt_flag]
    #---------------------------------------
    # get extra data needed
    # get extra dimensions and data needed
    if accpt_flag < 2:
        oil_price_dict = get_oilprices(cursor, schema="public_data", tablename="oil_price_monthly",
                                   ref_dates=data['report_date_mo'],
                                   input_oilprice_dict=oil_prices_full, API=data["API_prod"][0])
        data.update(oilprice=np.array(oil_price_dict['oilprice_value']))


    #Calculate per day values and also set 0's
    #data = calc_pd(data,month_end)

    #Remove outliers in oil water gas data
    #data = remove_spikes(data,5)
        data = moving_avg_outlier(data)

    return data

def moving_avg_outlier(data):
    """
    Calculate moving window average
    """
    keys = ["oil_pd", "wtr_pd", "gas_pd"]

    for val in keys:
        new_val = list(data[val]) #create a new list

        for i in xrange(2, len(new_val)-2):

            if new_val[i] > 10:
                o_temp = [new_val[i-2], new_val[i-1], new_val[i+1], new_val[i+2]]
                o_mean, o_std = np.mean(o_temp), np.std(o_temp)

                if new_val[i] > (o_mean+(3 * o_std)):
                    new_val[i] = round(o_mean + (3 * o_std), 2)

                if new_val[i] < (o_mean-(3 * o_std)):
                    new_val[i] = round(o_mean - (3 * o_std), 2)
        data[val] = new_val
        #data[val+"_mod"] = o

    return data


def trim_dictionary(data, trim_param): #last_prod_indx, first_prod_indx):
    """
    Trim if starting and ending have only zeros
    """
    first_prod_indx = next((i for i, x in enumerate(data[trim_param]) if x), 0)
    temp = next((i for i, x in enumerate(data[trim_param][::-1]) if x), 0)
    last_prod_indx = len(data[trim_param]) - temp

    for key in data:
        if isinstance(data[key], int):
            continue
        length = len(data[key])
        data[key] = data[key][np.nanmax(first_prod_indx, 0):np.nanmin([last_prod_indx, length])]

    first_prod_indx = 0 # update the first index of production
    last_prod_indx = len(data['oil_mo']) # update the last index of production

    return data, last_prod_indx, first_prod_indx


def calc_pd(data, month_end):
    """
    Calculate per day values
    """
    oil_pd, water_pd, gas_pd = [], [], []

    length = len(data["prod_days"])
    for i in xrange(length):

        #check non-zero
        if data["prod_days"][i] == 0 and data["oil_mo"][i] != 0:
            data["prod_days"][i] = month_end[i]

        elif data["oil_mo"][i] == 0 and data["gas_mo"][i] == 0:
            data["prod_days"][i] = 0

        temp_a, temp_b, temp_c = 0, 0, 0
        if data["prod_days"][i] != 0:
            temp_a = data["oil_mo"][i]/data["prod_days"][i]
            temp_b = data["wtr_mo"][i]/data["prod_days"][i]
            temp_c = data["gas_mo"][i]/data["prod_days"][i]

        oil_pd.append(round(temp_a, 2)), water_pd.append(round(temp_b, 2))
        gas_pd.append(round(temp_c, 2))

    data["oil_pd"], data["wtr_pd"] = np.asarray(oil_pd), np.asarray(water_pd)
    data["gas_pd"] = np.asarray(gas_pd)

    return data


def impute_missing_data(data_in):
    """
    Args:
    Accepts data for a single well and adds missing report_dates and corresponding production values

    data_in : A dictionary containing production data for one well.
              The values are lists, and all the keys should have the same length.

   Returns:
       data_out: Imputed data dictionary containing production data for one well.

       month_end : List of values containing end of month values for each report_date_mo
    """

    # determine the number of month between first and last dates given in data
    well_start_date = data_in['report_date_mo'][0]
    month_no_indx = np.array([diff_month(well_start_date, x) for x in data_in['report_date_mo']])

    # generate the continuous month dates strings for the new data
    allindx = range(max(month_no_indx)+1)
    dates_obj, month_end = add_months(well_start_date, allindx)

    # imput zeros for missing data
    data_out = data_in.copy()
    impute_keys = ['report_date_mo', "oil_mo", "wtr_mo", "gas_mo", "prod_days"]

    if "water_mo" in data_in.keys():
        impute_keys.append("water_mo")
    else:
        impute_keys.append("wtr_mo")

    for key in impute_keys:
        # Update the measured values series by imputing NaNs when data is missing
        data_out[key] = np.empty(shape=len(dates_obj), dtype=type(data_in[key]))
        #np.array(data_in[key],dtype=type(data_in[key][0]),ndmin=len(dates_obj))
        data_out[key].fill(0.0) #can change into zero if want 0

        if np.size(data_in[key]) == 1:
            length = len(data_out[key])

            #to handle wells with only one entry
            if np.size(data_in[key]) == np.size(data_out[key]):
                data_out[key] = data_in[key]
            else:
                data_out[key] = np.nan_to_num([data_in[key][0] for i in xrange(length)])

        else:
            data_out[key][month_no_indx] = np.nan_to_num(data_in[key])

        # replace nans with zeros
        data_out[key] = np.nan_to_num(data_out[key])

    #get linear time index of months and the continuous dates strings
    data_out['MonthIndx'] = np.asarray(range(max(month_no_indx)+1))  #range(month_no_indx[-1])
    data_out['report_date_mo'] = np.asarray(dates_obj)

    # Check that every key has list of data with equal length
    for key in data_out.keys():
        temp_m, temp_n = len(data_out[key]), len(data_out['report_date_mo'])

        if temp_m != temp_n:
            data_out[key] = [data_out[key][-1]]*temp_n

    return data_out, month_end



def diff_month(date1, date2):
    """Find difference in months between 2 dates """

    try:
        return (date2.year - date1.year)*12 + (date2.month - date1.month)
    except Exception as err:
        print err+" .The argument was {}.".format(date2)
        return np.NaN


def add_months(sourcedatestr, months):
    """Find and add report dates between a date and number of months given."""
    if isinstance(sourcedatestr, str):
        sourcedate = datetime.strptime(sourcedatestr, '%Y-%m-%d').date()
    else:
        sourcedate = sourcedatestr

    length_months = len(months)
    month = [sourcedate.month - 1 + months[i] for i in xrange(length_months)]
    year = [int(sourcedate.year + x / 12) for x in month]
    month = [month[i] % 12 + 1 for i in xrange(length_months)]

    # get the end of month day number date
    #length_year = len(year)
    day = [min(sourcedate.day, calendar.monthrange(year[i], month[i])[1])
           for i in xrange(length_months)]
    day = [calendar.monthrange(year[i], month[i])[1]
           for i in xrange(length_months)]
    dates = [datetime(year[i], month[i], day[i]).date()
             for i in xrange(length_months)]

    return dates, day


def get_oilprices(cursor, schema="public_data", tablename="oil_price_monthly",
                  ref_dates=None, input_oilprice_dict=None, API=None):
    """Get oil prices and sync it with the report dates for the current well"""

    if input_oilprice_dict is None:
        cursor.execute("SELECT * FROM "+schema+"."+tablename+";")
        row = cursor.fetchall()
        oil_prices = [row[i][1] for i in xrange(len(row))]

        oilprice_dates = [row[i][0] for i in xrange(len(row))]

    else:
        oil_prices = input_oilprice_dict['oilprice_value']
        oilprice_dates = input_oilprice_dict['oilprice_date']
    #del input_oilprice_dict


    if ref_dates is not None: # given set of dates for which oil priuce is needed
        ref_dates_obj = ref_dates
    else:
        ref_dates_obj = oilprice_dates

    #==== get the index of requested dates compared to ref dates in database
    istart = diff_month(oilprice_dates[0], ref_dates_obj[0])
    indx = range(istart, istart+len(ref_dates_obj))

    price_date = []
    price_val = []
    try:
        for val in indx:
            price_date.append(oilprice_dates[val])
            price_val.append(oil_prices[val])
    except Exception as error:
        print error
        print "Price length error", API

    oilprice_dict = defaultdict(list)
    oilprice_dict['oilprice_value'] = price_val
    oilprice_dict['oilprice_date'] = price_date

    return oilprice_dict


def down_time(prod_days, days_month=[], report_date_mo=[]):
    """This function calculates the number of days well
    has been down in each month. This function needs either report_date_mo
    or days_month NOT BOTH. Pass an empty list

    Args :
        report_date_mo : a list of production report dates.
                        The dates are datetime.date objects

        prod_days : a list of integers showing number of days the well produced

        days_month : a list of integers which give the number of days in that month

    Returns:
        days_down : a list of integers containing difference between total days of
                    the month & prod_days of the month

        days_month : a list of integers which give the number of days in that month
    """
    if len(report_date_mo) == 0 and len(days_month) == 0:
        print "Error. Check down_time function's parameters passed"
        sys.exit(0)

    if len(days_month) == 0:
        for date_val in report_date_mo:
            #array of Dates extract Month and Year from the string and
            #find the number of days in that month
            days_month.append(monthrange(date_val.year, date_val.month)[1])

    #days_down= (a-b for a,b zip(days_month,prod_days))
    days_down = map(sub, days_month, prod_days)
    return days_down, days_month



def Check_Valid_Producer(data, time_trsh, prod_trsh, diff_days, month_end):

    """
    this function checks whether the well is a valid producer for forecasting and gives
    # the index of when the well gone offline by checking the catastrophic failure criterias
    """
    flag, last_prod_indx = None, 0

    if bool(data) is False:
        #no data available
        flag = 6
        return flag, last_prod_indx, 0

    #Check the days down for given data
    oil = np.array(data['oil_mo'], float)
    water = np.array(data['wtr_mo'], float)
    gas = np.array(data['gas_mo'],float)
    welldates = data['report_date_mo']
    prod_days = np.array(data['prod_days'], float)
    days_down, days_in_month = down_time(prod_days, month_end)

    #calculate effective oil & effective gas

    eff_oil = sum(i > prod_trsh["oil"] for i in oil)
    eff_gas = sum(i > prod_trsh["gas"] for i in gas)

    if eff_oil >= time_trsh["oil"]:
        flag = 0
    elif eff_gas >= time_trsh["gas"]:
        flag = 1
    elif np.sum(np.abs(oil)) != 0 or np.sum(np.abs(gas)) != 0:
        flag = 2
    elif np.sum(np.abs(oil)) == 0 or np.sum(prod_days) == 0: #never produced
        flag = 3
        #return flag, last_prod_indx, 0
    elif np.sum(np.abs(oil)) == 0 and np.sum(np.abs(water)) != 0: # its an injector
        flag = 4
        #return flag, last_prod_indx, 0
    #elif min(len(welldates), len(oil), len(gas)) < min(time_trsh["oil"], time_trsh["gas"]):
        #flag = 5
        #return flag, last_prod_indx, 0
    else:
        flag = 5
        #return flag, last_prod_indx, 0

    first_prod_indx = 0
    return flag, len(prod_days), first_prod_indx

def write_metadata(conn, cursor, results_tablename, algorithm_name, algo_version, forecast_date, description, type_data):
    """
    Write information about the the table you are going to push which contains your forecasting results.
    This allows us to easily keep track of what each table contains.

    Args:
        conn : Database connection object

        cursor : Database connection object

        results_tablename : String. Name of the table to which you have written your results to

        algorithm_name : String. Algorithm that you used for forecasting

        algo_version : String. Algorithm version

        forecast date : String. Start date of forecast. Eg "2009-01-31"

        description : String. Brief description about the run

        type_data : String. Type of data the table contains. If it has forecasting results put "fore_results".

    Returns:
        Nothing

    """
    timestamp = datetime.strftime(datetime.now(), '%Y-%m-%dT%I:%M:%S %p')
    val = (results_tablename, algorithm_name, algo_version, forecast_date, str(timestamp), description, type_data)
    args_str = val
    #print args_str
    query = """INSERT INTO results."{}" VALUES {};""".format("#Forecast_table_tracker", args_str)
    cursor.execute(query)
    conn.commit()


def write_DB(conn, cursor, results_data, verbosity, schema, table_name, drop_table=False):
    """
    Write the results to the PostgreSQL database. The column names will be same
    as the keys in your data. All the data for a well must be of the same length,
    even non-timeseries
    attributes need to be of the same length, otherwise this method will give an
    Index out of range error. This function accepts both formats -
    List of dicts & Dict of lists.

    Type constraints -
        API_prod values need to be long objects
        report_date_mo values need to be datetime.date objects

    Args:
        conn : Database connection object

        cursor : Database connection object

        results_data : Data to be written to the database.
                       Can be a List of dictionarys or Dictionary of Lists.

        verbosity : Boolean flag. If true function will print summary of func execution to console.

        schema : String object. Name of schema in which to create the table.

        table_name : String object. Name of the database table to write data

        drop_table : Boolean object. If true will drop table of same table name if it exists

    Returns:
        Nothing

    """

    #convert it to Taha's format if its not already
    if type(results_data) == list:
        results_data = ld_to_dl(results_data)

    #results_data.pop("accptflag", None)

    # Drop table if exists
    if drop_table:
        try:
            query = """DROP TABLE IF EXISTS {}."{}";""".format(schema, table_name)
            cursor.execute(query)
            conn.commit()
        except Exception as error:
            print error
            print "Error while dropping table"

    results_keys = sorted(results_data.keys())
    temp_b = type(datetime.strptime('1900-01-01', '%Y-%m-%d').date()) #type check

    #Generate type dictionary for keys
    nos_items = len(results_data[results_keys[0]])

    type_dict = []
    for key in results_keys:
        try:
            value_type = None
            for well_data in xrange(nos_items):
                for well_value in xrange(len(results_data[key][well_data])):

                    if results_data[key][well_data][well_value] != None:
                        value_type = type(results_data[key][well_data][well_value])
                        break
                    else:
                        continue
                    break

        except Exception as error:
            print error
            print key, results_data[key]

        #if key has upper case characters, attach quotes
        if any(s.isupper() for s in key):
            key = "\""+key+"\""

        if value_type == None:
            type_dict.append(key+' VARCHAR(995)')
        elif value_type == bool:
            type_dict.append(key+' BOOLEAN')
        elif value_type == long:
            type_dict.append(key+' BIGINT')
        elif value_type == temp_b:
            type_dict.append(key+' DATE')
        elif value_type == str:
            type_dict.append(key+' VARCHAR(255)')
        else:
            #a == float or a == int or a == np.int32  :
            type_dict.append(key+' numeric NULL')

    structure = ",".join(type_dict)
    query = """CREATE TABLE IF NOT EXISTS {}."{}" ({})""".format(schema, table_name, structure)
    try:
        cursor.execute(query)
        conn.commit()
    except Exception as error:
        print error

    # Insert the rows
    values = list()
    count = 0

    if "API_master" in results_data.keys():
        key = "API_master"
    elif "API_prod" in results_data.keys():
        key = "API_prod"
    else:
        key = results_keys[0]

    #Build tuples out of the values for batch insert
    try:
        for outer_indx in xrange(len(results_data[key])):
            for inner_indx in xrange(len(results_data[key][outer_indx])):
                row_val = []
                for key in results_keys:

                    if str(results_data[key][outer_indx][inner_indx]) == 'None':
                        row_val.append(None)
                    else:
                        row_val.append(str(results_data[key][outer_indx][inner_indx]))
                count += 1
                values.append(tuple(row_val))
    except Exception as error:

        print error, inner_indx, outer_indx
        print "Length of all the values to be written should be the same"

    del results_data

    #Batch insert using mogrify approach
    template = "("+",".join(["%s"]*len(results_keys))+")"
    values = tuple(values)
    step_size = 25000
    n_batch = int(np.ceil(count/step_size))
    ind = [min(x*step_size, count) for x in range(n_batch+1)]

    start_time = time()
    for temp_indx in xrange(n_batch):
        args_str = ','.join(cursor.mogrify(template, val) for val in values[ind[temp_indx]:ind[temp_indx+1]])
        try:
            query = """INSERT INTO {}."{}" VALUES {};""".format(schema, table_name, args_str)
            cursor.execute(query)
            #cursor.execute("INSERT INTO "+schema+"."+table_name+" VALUES " + args_str)
        except Exception as error:
            print error

    if verbosity:
        print "Number of rows : ", count
        print "Insert time - "+ str(round((time()-start_time), 2))+" secs"
        print "-"*59
        print
    conn.commit()
    del values
    return

def create_index(conn, cursor, verbosity, schema, table_name, column, index_name):
    """
    Creates index on the database table
    Args :
        conn : Database connection object

        cursor : Database connection object

        verbosity : Boolean flag. If true function will print summary of func execution to console.

        schema : String object. Name of schema in which the table is written.

        table_name : String object. Name of the database table on which is to be indexed

        column : String object. Name of Column on which index is to be created

        index_name : String object. Unique index name.
                     Hint : Use a combination of column name & time-stamp to make it unique
                     if you cant think of anything else

    Returns:
        Nothing
    """
    try:
        query = """CREATE INDEX "{}" ON "{}"."{}" USING btree\
                ("{}");""".format(index_name, schema, table_name, column)
        if verbosity: print "Creating Index..."
        cursor.execute(query)
        conn.commit()
        if verbosity: print "Index created."

    except Exception as error:
        print error
        print "Index creation unsuccessful"

    return


# Stopped
def ld_to_dl(l_d):
    """
    Convert a list of dict to dict of lists
    """
    result = defaultdict(list)
    for data in l_d:
        #print data
        for key, val in data.items():
            result[key].append(val)
    return result


#Needs more testing

def univariate_lin_reg(temp_x, temp_y):
    """
    Performs simple linear regression and reports the intercept.
    The inputs x and y are column vectors.
    """
    lam = 0.001
    r_term = sum(temp_y*temp_x)/len(temp_x) - temp_y.mean()/temp_x.mean()*(np.mean(temp_x**2)+lam)
    l_term = temp_x.mean() - (np.mean(temp_x**2)+lam)/temp_x.mean()
    return r_term/l_term


def remove_spikes(data, lwin):
    """Jumpfinder function to remove peaks. Needs more testing"""

    keys = ['oil_mo', 'wtr_mo', 'gas_mo']

    for k in keys:
        old_ts = data[k]
        data[k+"_original"] = list(data[k])

        for temp_t in range(lwin, len(data[k])-lwin):
            point = data[k][temp_t]

            # Get the left estimate
            l_est = univariate_lin_reg(np.arange(-lwin, 0), old_ts[temp_t-lwin:temp_t])

            # Get the right estimate
            r_est = univariate_lin_reg(np.arange(1, lwin+1), old_ts[temp_t+1:temp_t+lwin+1])
            # if min(abs(l_est-point), abs(r_est-point))/(l_est+r_est) > 0.2:
            # If the normalized amount of jump is larger than a ratio, replace
            # it by the average of the estimates.
            if min(max(point-l_est, 0), max(point-r_est, 0))/(l_est+r_est+EPS) > 0.4:
                data[k][temp_t] = (l_est+r_est)/2

    return data

#Tertiary methods just used for testing purposes

def write_csv(temp_a, temp_b):
    """write two lists to CSVs"""
    result_file = open("Worthy_APIs_new.csv", 'wb')
    wr_obj = csv.writer(result_file)
    for j in temp_a:
        wr_obj.writerow([j])
    result_file.close()

    result_file = open("Not_Worthy_APIs_new.csv", 'wb')
    wr_obj = csv.writer(result_file)
    for k in temp_b:
        wr_obj.writerow([k])
    result_file.close()


def write_data_csv(data):
    """Write data to csv to check moving avg outlier results"""
    result_file = open(str(data["API_prod"][0])+"_2.csv", 'wb')
    wr_obj = csv.writer(result_file)

    for i in range(len(data['report_date_mo'])):

        wr_obj.writerow([data['report_date_mo'][i], data["prod_days"][i],
                         data["oil_mo"][i], data["oil_mo_mod"][i]])

    result_file.close()
