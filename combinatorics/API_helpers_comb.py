# -*- coding: utf-8 -*-
"""
Created on Sat Nov 28 16:03:16 2015
Combinatorics helper
Reading from the database
@author: Administrator
"""
from datetime import datetime
import calendar
import numpy as np
import MySQLdb

def establish_db_conn(user, password, schema="public_data"):
    """
    Establish connection with the database using the given parameters.
    """
    cnx = {'host': 'navjot.c4lstuf6msdr.us-west-2.rds.amazonaws.com',\
    'user':user, 'password': password, 'db': schema}
    conn = MySQLdb.connect(cnx['host'], cnx['user'], cnx['password'], cnx['db'])
    cursor = conn.cursor()
    return cursor


def get_well_ids(cursor, table_name, schema="public_data"):
    """
    Gets the well ids from the table data is being retrieved.
    """
    #start=time.time()
    print "Reading well IDs"
    query = "SELECT distinct(API_prod) FROM "+schema+"."+table_name+";"
    cursor.execute(query)
    row = cursor.fetchall()
    unique_pids = [v[0] for v in row]

    return sorted(unique_pids)

def get_well_data(cursor, well_id, table_name, schema="public_data"):
    """
    Reads the data from the the MySql DB table.
    """
    #start=time.time()
    query = "SELECT oil,water,prod_days,report_date \
             FROM "+schema+"."+table_name+" WHERE API_prod=%s ORDER BY report_date ASC ;"
    cursor.execute(query, [well_id])
    row = cursor.fetchall()

    well_dates, proddays, qoil, qwat = [], [], [], []
    """
    Make single lists for each column they are bound by index.
    """

    for value in row:
        qoil.append(value[0])
        qwat.append(value[1])
        proddays.append(int(value[2]))
        well_dates.append(value[3].strftime("%Y-%m-%d")) # extract operating dates for this well
    #print("Time Taken : "+str(round(((time.time()-start)),2))+" secs")
    #print

    return well_dates, proddays, qoil, qwat

def diff_month(date1, date2):
    """
    Find difference in months between 2 dates
    """
    try:
        return (date2.year - date1.year)*12 + (date2.month - date1.month)
    except Exception as err:
        print err+" .The argument was {}.".format(date2)
        return np.NaN


def add_months(sourcedate, months):
    ###sourcedate = datetime.strptime(sourcedatestr , '%Y-%m-%d')

    month = [sourcedate.month - 1 + months[i] for i in xrange(len(months))]
    year = [int(sourcedate.year + x / 12) for x in month]
    month = [month[i] % 12 + 1 for i in xrange(len(month))]

    length_year = len(year)
    day = [min(sourcedate.day, calendar.monthrange(year[i], month[i])[1])
           for i in xrange(length_year)]
    day = [calendar.monthrange(year[i], month[i])[1]
           for i in range(length_year)] # get the end of month day number date

    datestring = [(str(year[i])+"-"+str(month[i])+"-"+str(day[i])) for i in range(len(year))]
    dates = [datetime.strptime(datestring[i], '%Y-%m-%d').date() for i in range(len(datestring))]
    return datestring, dates


def impute_missing_data(dataIn):
    """finds missing months in the Welldates and inserts 0
    for qoil, qwat at the corresponding indexes"""

    # determine the number of month between first and last dates given in data
    well_start_date = dataIn['dates'][0]
    month_no_indx = np.array([diff_month(well_start_date, x) for x in dataIn['dates']])

    # generate the continuous month dates strings for the new data
    allindx = range(max(month_no_indx)+1)
    datesstr, date_obj = add_months(well_start_date, allindx)

    # imput nans for missing data
    date_out = dataIn.copy()
    for (i, key) in enumerate(dataIn):
        # Update the measured values series by imputing NaNs when data is missing
        date_out[key] = np.empty(shape=len(date_obj), dtype=type(dataIn[key]))
        #np.array(dataIn[key],dtype=type(dataIn[key][0]),ndmin=len(date_obj ))
        date_out[key].fill(0.0) # can change into zero if want 0
        date_out[key][month_no_indx] = dataIn[key]


    # get linear time index of months and the continuous dates strings
    date_out['MonthIndx'] = np.asarray(range(np.nanmax(month_no_indx)+1))  #range(month_no_indx[-1])
    date_out['dates'] = np.asarray(date_obj)

    return date_out


def inverse_cumsum(cumulative):
    """given a cumulative find the original values"""
    output = [0] * len(cumulative)

    for i in xrange(len(cumulative)-1):
        output[i+1] = cumulative[i+1] - cumulative[i]
    output[0] = cumulative[0]

    return output

def matches(temp_a, temp_b):
    """function to objectively test how similar the decoded values are of qoil and qwat"""
    temp_c = [i for i, j in zip(temp_a, temp_b) if i == j]
    return round((float(len(temp_c))/len(temp_a))*100, 2)
