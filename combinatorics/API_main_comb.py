# -*- coding: utf-8 -*-
"""
Created on Sat Nov 28 16:03:19 2015
Combinatorics script modified to behave as an API
@author: Administrator
"""
from __future__import division
from math import log, exp
import sys
import numpy as np

from API_helpers_comb import inverse_cumsum
import terraai_preprocessing.preprocessing.pp_helpers as pp_helpers

__version__ = 1.36

def encoder(API_prod, report_date_mo, prod_days, oil_mo, wtr_mo, request_args, scale_args, impute_data=False):
    """
    Take the input parameters, request args and scale args.
    Then apply the appropriate transformations on them, returning encoded data
    with an opcode indicating transformations.

    Args:
        API_prod : A list containing API_prod of the current well.
        report_date_mo : A list containing report_date_mo of the current well
        prod_days : A list containing prod_days data
        oil_mo : A list containing oil production values
        wtr_mo : A list containing water values
        request args : A list with two values that you want to encode.
                       Its case insensitive. Options are ['time','oil_mo',
                       'wtr_mo','wor','cumoil','cumwat','gross','fo','fw']
        scale_args : A list with two values that specify the scale of the
                     values in request_args.
                     Options are ['linear','log']. scale_args[0] will be
                     applicable to request_args[0]
                     while scale_args[1] will be applicable to request_args[1].
        impute_data : A flag which accepts boolean objects True/False. If set to
                      True the function will find & impute missing data by adding 0's.

    Returns:
        encoded_data : A dictionary with the following keys -
            API_prod : A list containing API_prod of the current well.
            report_date_mo : A list containing report_date_mo of the current well.
            prod_days : A list containing prod_days data
            arg1, arg2 : Contains data specified by request_args with their
                         scale conversions.
            opcode : Integer. A four digit code that specifies the transformation applied on the
                     data supplied to the encoder.



    """
    #if arguments are from command line
    #arg1,arg2,arg3,arg4 = sys.argv[1],sys.argv[2],sys.argv[3],sys.argv[4]

     #if arguments are specified in function call
    request_args = map(lambda x: x.lower(), request_args)
    scale_args = map(lambda x: x.lower(), scale_args)
    arg1, arg2, arg3, arg4 = request_args[0], request_args[1], scale_args[0], scale_args[1]

    if arg1 == arg2:
        print "Error : Both request parameters cannot be same"
        sys.exit(0)


    if (arg1 == "time" and arg3 == "log") or (arg2 == "time" and arg4 == "log"):
        print "Error : Time cannot be taken on a 'log' scale"
        sys.exit(0)

    #variable declaration
    math_func = ['linear', 'log']
    arg_list = ['time', 'oil_mo', 'wtr_mo', 'wor', 'cumoil', 'cumwat', 'gross', 'fo', 'fw']

    if arg1 not in arg_list or arg2 not in arg_list:
        print "Error : Check request arguments"
        sys.exit(0)

    if arg3 not in math_func or arg4 not in math_func:
        print "Error : Check scale arguments"
        sys.exit(0)

    arg_value = {key:[] for key in arg_list}

    if impute_data:
        #find missing monthly dates and add 0's
        x = pp_helpers.impute_missing_data({"dates":report_date_mo, "oil_mo":oil_mo,
                                            "wtr_mo":wtr_mo, "prod_days":prod_days})
    else:
        x = {"dates":report_date_mo, "oil_mo":oil_mo, "wtr_mo":wtr_mo, "prod_days":prod_days}

    if type(x["oil_mo"]) == np.ndarray:
        x["oil_mo"] = x["oil_mo"].tolist()

    if type(x["wtr_mo"]) == np.ndarray:
        x["wtr_mo"] = x["wtr_mo"].tolist()

    arg_value["oil_mo"] = map(np.abs, x["oil_mo"])
    arg_value["wtr_mo"] = map(np.abs, x["wtr_mo"])

    #update oil_mo and wtr_mo variables so that when log is
    #applied they remain constant as have to be returned
    oil_mo = arg_value["oil_mo"]
    wtr_mo = arg_value["wtr_mo"]

    arg_value["time"] = x["dates"]

    if type(x["prod_days"]) == np.ndarray:
        x["prod_days"] = x["prod_days"].tolist()
    arg_value["prod_days"] = x["prod_days"]

    #Calculate secondary parameters based of primary oil_mo, wtr_mo
    if arg1 == "cumoil" or arg2 == "cumoil":
        cumoil = np.cumsum(arg_value["oil_mo"])
        arg_value["cumoil"] = cumoil.tolist()

    if arg1 == "cumwat" or arg2 == "cumwat":
        cumwat = np.cumsum(arg_value["wtr_mo"])
        arg_value["cumwat"] = cumwat.tolist()

    if arg1 == "wor" or arg2 == "wor":
        with np.errstate(divide='ignore', invalid='ignore'):
            c = np.true_divide(arg_value["wtr_mo"], arg_value["oil_mo"])
            c[c == np.inf] = 0
            c = np.nan_to_num(c)
        arg_value["wor"] = c

        #arg_value["wor"]=[a/b if b!=0.0 else 0.0 for float(a),float(b)
        # in zip(arg_value["wtr_mo"],arg_value["oil_mo"])]

    if arg1 == "gross" or arg2 == "gross" or arg1 == "fw" or arg2 == "fw"\
        or arg1 == "fo" or arg2 == "fo":
        arg_value["gross"] = [a + b for a, b in zip(arg_value["wtr_mo"], arg_value["oil_mo"])]

        if arg1 == "fw" or arg2 == "fw":
            arg_value["fw"] = [float(a)/b if b != 0 else 0
                               for a, b in zip(arg_value["wtr_mo"], arg_value["gross"])]

        if arg1 == "fo" or arg2 == "fo":
            arg_value["fo"] = [float(a)/b if b != 0 else 0
                               for a, b in zip(arg_value["oil_mo"], arg_value["gross"])]

    """adding +1 to indexes so that it displays proper integer code and does
        not skip leading zeros when converting to integer. """
    code = int(str(arg_list.index(arg1)+1)+str(arg_list.index(arg2)+1)\
               +str(math_func.index(arg3)+1)+str(math_func.index(arg4)+1))

    #for cases when value is 0 we add 1 and in exp we subtract 1
    if arg3 == "log":

        y = [log(a+1) for a in arg_value[arg1]] #if a!=0 else 0
        arg_value[arg1] = y

    if arg4 == "log":

        x = [log(a+1) for a in arg_value[arg2]] #if a!=0 else 0
        arg_value[arg2] = x

    return {"API_prod":API_prod, "arg1":arg_value[arg1], "arg2":arg_value[arg2],\
            "opcode":code, "report_date_mo":arg_value["time"],\
            "prod_days":arg_value["prod_days"]} #,"oil_mo":oil_mo,"wtr_mo":wtr_mo}


def decoder(encoded_data):

    """Decodes the data to retrieve original values.

        Args -
            encoded_data : A dictionary having the same keys as those returned
                            by the encoder function

        Returns -
            decoded_data : a dictionary with the following keys
                API_prod: API_prod ID of the well
                report_date_mo: report_dates of the well, stored as a list
                prod_days: prod_days of the well, stored as a list
                x,y: x and y are the original request_args supplied to encoder().
                oil_mo, wtr_mo : These values will be returned if they can be reverse
                                engineered from the request_args.
                                Possible with permutation of request args being
                                ['oil_mo','wtr_mo','wor','cumoil','cumwat','gross'],
                                barring [oil_mo,cumoil] and [wtr_mo,cumwat]
    """

    arg_list = ['time', 'oil_mo', 'wtr_mo', 'wor', 'cumoil', 'cumwat', 'gross', 'fo', 'fw']

#    split the code into individual numbers to determine
#    further process. also subtract 1 to get arg_list indexes
    code = encoded_data["opcode"]
    code_list = []
    while len(str(code)) != 1:
        code_list.append(code%10-1)
        code = code/10
    code_list.append(code-1)

    #Reverse the code list to get the correct order
    code_list = code_list[::-1]
    #print code_list

    #find the names of the starting code values from the arg_list so they
    #act as keys in the decoded_values dictionary
    x, y = arg_list[code_list[0]], arg_list[code_list[1]]

    #declare dictionary to store the return values
    decoded_values = {"API_prod":encoded_data["API_prod"]}

    #if time (Welldates) is not present in the code add it to the decoded_values dictionary
    if x != 0 and y != 0:
        decoded_values["report_date_mo"] = encoded_data["report_date_mo"]
    decoded_values["prod_days"] = encoded_data["prod_days"]
    #Take inverse log if log conversion was done else just add it to the dictionary
    # 1 = log  0 = linear scale
    if code_list[2] == 1:
        decoded_values[x] = [round((exp(a)-1), 6) for a in encoded_data['arg1']]
        """print "Inverse Log WOR "
        print decoded_values[x]
        print"""
    elif code_list[2] == 0:
        decoded_values[x] = encoded_data['arg1']

    if code_list[3] == 1:
        decoded_values[y] = [round((exp(a)-1), 6) for a in encoded_data['arg2']]
    elif code_list[3] == 0:
        decoded_values[y] = encoded_data['arg2']

    #Reverse engineer values of oil_mo and wtr_mo from these 24 possible permutations.
    perm = [(1, 2), (2, 1), (1, 3), (1, 5), (1, 6), (2, 3), (2, 4), (2, 6),
            (3, 1), (3, 2), (3, 4), (3, 5), (4, 2), (4, 3), (4, 5), (4, 6),
            (5, 1), (5, 3), (5, 4), (5, 6), (6, 1), (6, 2), (6, 4), (6, 5)]

    #Check if the the code is in above permutations to reverse engineer oil_mo and wtr_mo
    if (code_list[0], code_list[1]) in perm or (code_list[1], code_list[0]) in perm:

        #if cumoil exists find oil_mo from it
        if code_list[0] == 4 or code_list[1] == 4:

            decoded_values["oil_mo"] = inverse_cumsum(decoded_values[arg_list[4]])

        #if cumwat exists find wtr_mo from it
        if code_list[0] == 5 or code_list[1] == 5:
            decoded_values["wtr_mo"] = inverse_cumsum(decoded_values[arg_list[5]])

        #if wor exists Find oil_mo(wtr_mo) given wor and wtr_mo(oil_mo).
        if code_list[0] == 3 or code_list[1] == 3:

            if "oil_mo" in decoded_values.keys():
                decoded_values["wtr_mo"] = [float(round(a*b, 3)) for a, b in\
                                          zip(decoded_values["oil_mo"], decoded_values["wor"])]

            elif "wtr_mo" in decoded_values.keys():
                decoded_values["oil_mo"] = [float(round((float(a)/b), 3)) if b != 0 else 0 for a, b in\
                                          zip(decoded_values["wtr_mo"], decoded_values["wor"])]

        #if gross exists
        if code_list[0] == 6 or code_list[1] == 6:

            if "oil_mo" in decoded_values.keys():
                decoded_values["wtr_mo"] = [a - b for a, b in\
                                          zip(decoded_values["gross"], decoded_values["oil_mo"])]
            elif "wtr_mo" in decoded_values.keys():
                decoded_values["oil_mo"] = [a - b for a, b in\
                                          zip(decoded_values["gross"], decoded_values["wtr_mo"])]
    else:
        pass
    #print "Cannot reverse engineer oil_mo and wtr_mo from the given arguments"
    return decoded_values
