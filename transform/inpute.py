import numpy as np

def handle_missing(dates, oil, water, gas, days):
    """
    This function handles the missing data points by inserting zeros in the missing months.
    """
    new_dates = np.arange(min(dates), max(dates)+1)
    new_oil, new_water, new_gas, new_days = [np.zeros(new_dates.shape) for _ in range(4)]
    new_oil[dates-min(dates)] = oil
    new_water[dates-min(dates)] = water
    new_days[dates-min(dates)] = days
    new_gas[dates-min(dates)] = gas
    return new_dates, new_oil, new_water, new_gas, new_days

def impute_missing_data(data_in):
    """
    Args:
    Accepts data for a single well and adds missing report_dates and corresponding production values

    data_in : A dictionary containing production data for one well.
              The values are lists, and all the keys should have the same length.

   Returns:
       data_out: Imputed data dictionary containing production data for one well.

       month_end : List of values containing end of month values for each report_date_mo
    """

    # determine the number of month between first and last dates given in data
    well_start_date = data_in['report_date_mo'][0]
    month_no_indx = np.array([diff_month(well_start_date, x) for x in data_in['report_date_mo']])

    # generate the continuous month dates strings for the new data
    allindx = range(max(month_no_indx)+1)
    dates_obj, month_end = add_months(well_start_date, allindx)

    # imput zeros for missing data
    data_out = data_in.copy()
    impute_keys = ['report_date_mo', "oil_mo", "wtr_mo", "gas_mo", "prod_days"]

    if "water_mo" in data_in.keys():
        impute_keys.append("water_mo")
    else:
        impute_keys.append("wtr_mo")

    for key in impute_keys:
        # Update the measured values series by imputing NaNs when data is missing
        data_out[key] = np.empty(shape=len(dates_obj), dtype=type(data_in[key]))
        #np.array(data_in[key],dtype=type(data_in[key][0]),ndmin=len(dates_obj))
        data_out[key].fill(0.0) #can change into zero if want 0

        if np.size(data_in[key]) == 1:
            length = len(data_out[key])

            #to handle wells with only one entry
            if np.size(data_in[key]) == np.size(data_out[key]):
                data_out[key] = data_in[key]
            else:
                data_out[key] = np.nan_to_num([data_in[key][0] for i in xrange(length)])

        else:
            data_out[key][month_no_indx] = np.nan_to_num(data_in[key])

        # replace nans with zeros
        data_out[key] = np.nan_to_num(data_out[key])

    #get linear time index of months and the continuous dates strings
    data_out['MonthIndx'] = np.asarray(range(max(month_no_indx)+1))  #range(month_no_indx[-1])
    data_out['report_date_mo'] = np.asarray(dates_obj)

    # Check that every key has list of data with equal length
    for key in data_out.keys():
        temp_m, temp_n = len(data_out[key]), len(data_out['report_date_mo'])

        if temp_m != temp_n:
            data_out[key] = [data_out[key][-1]]*temp_n

    return data_out, month_end

