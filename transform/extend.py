from conns import *
import numpy as np
from sklearn.linear_model import Ridge
from tqdm import tqdm   # For progress bar

"""
Strategy:
1. A pre extension and post extension would be helpful if it was persisted. 
2. 

TTD 
1. Bring in results from 
2. 

"""


def read_result(well, table):
    query = """SELECT report_date_mo, oil_pd_fore_mean from results."{}" where "API_prod"={}""".format(table, well)
    conn = psycopg2.connect(database='terra', user=config['user'], password=config['password'], host=config['host'], port=config['port'])
    cursor = conn.cursor()
    cursor.execute(query)
    row = cursor.fetchall()
    dates = [diff_month_date(x[0]) for x in row]
    target = [mynum(float, x[1], np.nan) for x in row]
    dates, target = zip(*sorted(zip(dates, target)))
    cursor.close()
    conn.close()
    return dates, target

def write_extended_result(well, table, dates, target):
    dates = [add_months(x) for x in dates]
    conn = psycopg2.connect(database='terra', user=config['user'], password=config['password'], host=config['host'], port=config['port'])
    cursor = conn.cursor()
    values = ','.join(["({},{},'{}'::date)".format(well, target[i], dates[i]) for i in range(len(target))])
    query = """INSERT INTO results."{}"("API_prod", oil_pd_fore_mean, report_date_mo) Values{};""".format(table, values)
    cursor.execute(query)
    conn.commit()
    cursor.close()
    conn.close()

def extend_one(well, table, setting):
    # Read the data
    dates, target = read_result(well, table)
    # Prepare data for linear regression
    n_fore = sum([~np.isnan(x) for x in target])
    n_train = min(n_fore, setting['max_len'])
    x_train = np.array(dates[-n_train:])
    y_train = np.array(target[-n_train:])
    y_train = np.log10(y_train+1) # Go to the log domain
    y_train[~np.isfinite(y_train)] = 0
    reg = Ridge(alpha=1e-2).fit(x_train[:, None], y_train)
    if setting['mode'] == 'fixed':
        elen = setting['par']
    elif setting['mode'] == 'economic':
        beta1, beta0 = reg.coef_[0], reg.intercept_
        if (beta1 >= 0) and (not setting['go_up']):
            elen = 1 # The forecast is not good, don't extend.
        else:
            eclim = np.log10(setting['par']+1)
            elen = int(np.ceil((eclim-beta0)/beta1)) - dates[-1]
    else:
        raise Exception('Not an option for extension.')
    elim = min(setting['limit'], elen)
    x_test = dates[-1] + np.arange(1, elen+1)
    y_pred = reg.predict(x_test[:, None])
    y_pred = np.power(10, y_pred)-1
    y_pred[y_pred < 0] = 0
    # Write to db
    write_extended_result(well, table, x_test.tolist(), y_pred.tolist())



def extend(table, setting):
    query = """SELECT DISTINCT "API_prod" from results."{}" """.format(table)
    conn = psycopg2.connect(database='terra', user=config['user'], password=config['password'], host=config['host'], port=config['port'])
    cursor = conn.cursor()
    cursor.execute(query)
    row = cursor.fetchall()
    well_ids = [x[0] for x in row]
    print "Extending the forecasts..."
    for well in tqdm(well_ids):
        try:
            extend_one(well, table, setting)
        except ValueError:
            print "Error in extending well: {}.".format(well)
    cursor.close()
    conn.close()