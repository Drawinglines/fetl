
import numpy as np


def univariate_lin_reg(temp_x, temp_y):
    """
    Performs simple linear regression and reports the intercept.
    The inputs x and y are column vectors.
    """
    lam = 0.001
    r_term = sum(temp_y*temp_x)/len(temp_x) - temp_y.mean()/temp_x.mean()*(np.mean(temp_x**2)+lam)
    l_term = temp_x.mean() - (np.mean(temp_x**2)+lam)/temp_x.mean()
    return r_term/l_term

def remove_spikes(data, lwin):
    """Jumpfinder function to remove peaks. Needs more testing"""

    keys = ['oil_mo', 'wtr_mo', 'gas_mo']

    for k in keys:
        old_ts = data[k]
        data[k+"_original"] = list(data[k])

        for temp_t in range(lwin, len(data[k])-lwin):
            point = data[k][temp_t]

            # Get the left estimate
            l_est = univariate_lin_reg(np.arange(-lwin, 0), old_ts[temp_t-lwin:temp_t])

            # Get the right estimate
            r_est = univariate_lin_reg(np.arange(1, lwin+1), old_ts[temp_t+1:temp_t+lwin+1])
            # if min(abs(l_est-point), abs(r_est-point))/(l_est+r_est) > 0.2:
            # If the normalized amount of jump is larger than a ratio, replace
            # it by the average of the estimates.
            if min(max(point-l_est, 0), max(point-r_est, 0))/(l_est+r_est+EPS) > 0.4:
                data[k][temp_t] = (l_est+r_est)/2

    return data