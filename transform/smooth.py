import numpy as np

def smooth(yhat, setting):
    enum = np.arange(yhat.shape[1])
    smoother = np.exp(-abs(enum[None, :] - enum[:, None])/setting['smth'])
    #Taha - What happens when we increase smth to beyond 1? Also what happens on the first tie in point to the smoothing?
    smoother /= np.sum(smoother, axis=0, keepdims=True)
    return np.dot(yhat, smoother)