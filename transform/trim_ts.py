def trim_dictionary(data, trim_param): #last_prod_indx, first_prod_indx):
    """
    Trim if starting and ending have only zeros
    """
    first_prod_indx = next((i for i, x in enumerate(data[trim_param]) if x), 0)
    temp = next((i for i, x in enumerate(data[trim_param][::-1]) if x), 0)
    last_prod_indx = len(data[trim_param]) - temp

    for key in data:
        if isinstance(data[key], int):
            continue
        length = len(data[key])
        data[key] = data[key][np.nanmax(first_prod_indx, 0):np.nanmin([last_prod_indx, length])]

    first_prod_indx = 0 # update the first index of production
    last_prod_indx = len(data['oil_mo']) # update the last index of production

    return data, last_prod_indx, first_prod_indx