from datetime import datetime


def diff_month(date_in):
    """
    Calculates the difference (date_in-initDate) in terms of number of months in between them.
    """
    try:
        if not isinstance(date_in, datetime): # Parameter overloading
            date_in = datetime.strptime(date_in, '%Y-%m-%d')
        return (date_in.year - initDate.year)*12 + date_in.month - initDate.month
    except ValueError:
        pdb.set_trace()
        print "ValueError: The argument was {}.".format(date_in)
        return 0


def diff_month_date(date_in):
    return (date_in.year - initDate.year)*12 + date_in.month - initDate.month

##  pp_helpers
def diff_month(date1, date2):
    """Find difference in months between 2 dates """

    try:
        return (date2.year - date1.year)*12 + (date2.month - date1.month)
    except Exception as err:
        print err+" .The argument was {}.".format(date2)
        return np.NaN


def add_months(months):
    """
    Returns the date for several months after the initDate.
    """
    month = initDate.month - 1 + months
    year = int(initDate.year + month / 12)
    month = month % 12 + 1
    return date(year, month, monthrange(year, month)[1])  # Reporting on the last day of the month


##  pp_helpers
def add_months(sourcedatestr, months):
    """Find and add report dates between a date and number of months given."""
    if isinstance(sourcedatestr, str):
        sourcedate = datetime.strptime(sourcedatestr, '%Y-%m-%d').date()
    else:
        sourcedate = sourcedatestr

    length_months = len(months)
    month = [sourcedate.month - 1 + months[i] for i in xrange(length_months)]
    year = [int(sourcedate.year + x / 12) for x in month]
    month = [month[i] % 12 + 1 for i in xrange(length_months)]

    # get the end of month day number date
    #length_year = len(year)
    day = [min(sourcedate.day, calendar.monthrange(year[i], month[i])[1])
           for i in xrange(length_months)]
    day = [calendar.monthrange(year[i], month[i])[1]
           for i in xrange(length_months)]
    dates = [datetime(year[i], month[i], day[i]).date()
             for i in xrange(length_months)]

    return dates, day

