def process_data(cursor, data, time_trsh, prod_trsh, diff_days, oil_prices_full, flag_dict):
    """
    Impute data, check if valid producer, get data from combinatorics.
    """
#    flag_dict = {0:"Fully producing well", 1:"No oil but significant gas",
#                 2:"No significant oil or gas", 3:"Injector well",
#                 4:"Inconsistent well with the logic", 5:"No data available",
#                 6:"Produced less then threshold", 7:"Well abandoned"}

    if bool(data):
        #impute new data if some months are missing and add the corresponding dates
        data, month_end = impute_missing_data(data)

    data = calc_pd(data, month_end)

    #Check if it is a valid producer for forecasting
    accpt_flag, last_prod_indx, first_prod_indx = Check_Valid_Producer(data, time_trsh, prod_trsh,
                                                                      diff_days, month_end)
    if accpt_flag > 2 and accpt_flag != 5:
        data["accpt_flag"] = accpt_flag
        data["reason"] = flag_dict[accpt_flag]
        return data

    trim_param = "oil_mo" if accpt_flag==0 or accpt_flag==5 else "gas_mo"
    data, last_prod_indx, first_prod_indx = trim_dictionary(data,trim_param)
                                                            #last_prod_indx, first_prod_indx)


    ###accptflag,last_prod_indx,first_prod_indx = Check_Valid_Producer(data,
                                                #time_trsh,diff_days) #to check on trim data
    if last_prod_indx - first_prod_indx < time_trsh[trim_param.replace("_mo","")]:
        #if well has been a producer less this number of months only
        accpt_flag = 7
        #return 0

    data["accpt_flag"] = accpt_flag
    data["reason"] = flag_dict[accpt_flag]
    #---------------------------------------
    # get extra data needed
    # get extra dimensions and data needed
    if accpt_flag < 2:
        oil_price_dict = get_oilprices(cursor, schema="public_data", tablename="oil_price_monthly",
                                   ref_dates=data['report_date_mo'],
                                   input_oilprice_dict=oil_prices_full, API=data["API_prod"][0])
        data.update(oilprice=np.array(oil_price_dict['oilprice_value']))


    #Calculate per day values and also set 0's
    #data = calc_pd(data,month_end)

    #Remove outliers in oil water gas data
    #data = remove_spikes(data,5)
        data = moving_avg_outlier(data)

    return data

def moving_avg_outlier(data):
    """
    Calculate moving window average
    """
    keys = ["oil_pd", "wtr_pd", "gas_pd"]

    for val in keys:
        new_val = list(data[val]) #create a new list

        for i in xrange(2, len(new_val)-2):

            if new_val[i] > 10:
                o_temp = [new_val[i-2], new_val[i-1], new_val[i+1], new_val[i+2]]
                o_mean, o_std = np.mean(o_temp), np.std(o_temp)

                if new_val[i] > (o_mean+(3 * o_std)):
                    new_val[i] = round(o_mean + (3 * o_std), 2)

                if new_val[i] < (o_mean-(3 * o_std)):
                    new_val[i] = round(o_mean - (3 * o_std), 2)
        data[val] = new_val
        #data[val+"_mod"] = o

    return data