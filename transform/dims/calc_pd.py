def calc_pd(data, month_end):
    """
    Calculate per day values
    """
    oil_pd, water_pd, gas_pd = [], [], []

    length = len(data["prod_days"])
    for i in xrange(length):

        #check non-zero
        if data["prod_days"][i] == 0 and data["oil_mo"][i] != 0:
            data["prod_days"][i] = month_end[i]

        elif data["oil_mo"][i] == 0 and data["gas_mo"][i] == 0:
            data["prod_days"][i] = 0

        temp_a, temp_b, temp_c = 0, 0, 0
        if data["prod_days"][i] != 0:
            temp_a = data["oil_mo"][i]/data["prod_days"][i]
            temp_b = data["wtr_mo"][i]/data["prod_days"][i]
            temp_c = data["gas_mo"][i]/data["prod_days"][i]

        oil_pd.append(round(temp_a, 2)), water_pd.append(round(temp_b, 2))
        gas_pd.append(round(temp_c, 2))

    data["oil_pd"], data["wtr_pd"] = np.asarray(oil_pd), np.asarray(water_pd)
    data["gas_pd"] = np.asarray(gas_pd)

    return data