SELECT ts."API_prod", ts.report_date_mo, ts.oil_pd, ts.water_pd, ts.gas_pd, ts.prod_days_up, ts.lat, ts.longi, ts.depth, ts.pool_name
    from public_data.bigprod_ts ts where ts."API_prod" in(SELECT distinct "API_prod" from public_data.cali_prod p1
    INNER JOIN public_data.cali_master m1 ON p1."API_prod" = m1."API_master" WHERE p1."API_prod" not in(SELECT count(distinct "API_master")
        FROM public_data.cali_master m INNER JOIN
        (SELECT * FROM public_data.cali_prod ps WHERE (ps.oil_mo = 0 and ps.report_date_mo = '2000-12-31')) p ON m."API_master" = p."API_prod"
        WHERE m.field_name IN('Kern River', 'Kern Front', 'Mount Poso', 'San Ardo'))
        and m1. field_name IN('Kern River', 'Round Mountain', 'Kern Front', 'Mount Poso', 'San Ardo'))



